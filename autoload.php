<?PHP
$loadedClasses = 0;
  function __autoload($class_name){
      $imported = false;
      $namespaces = explode("_",strtolower($class_name));
      if(sizeOf($namespaces) == 3){
        if($namespaces[0] == "plugin"){
          if(file_exists("system/plugins/".$namespaces[1]."/classes/".$namespaces[2].".php")){
            require_once "system/plugins/".$namespaces[1]."/classes/".$namespaces[2].".php";
            $imported = true;
          }
        }
        else if($namespaces[0] == "skin"){
          if(file_exists("system/skins/".$namespaces[1]."/classes/".$namespaces[2].".php")){
            require_once "system/skins/".$namespaces[1]."/classes/".$namespaces[2].".php";
            $imported = true;
          }
        }
      }
      if(!$imported){
          if(file_exists("system/classes/".strtolower($class_name).".php")){
            require_once "system/classes/".strtolower($class_name).".php";
          }
      }
      $GLOBALS['loadedClasses']++;
  }
?>