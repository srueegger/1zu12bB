<?php
  function callback_trim($buffer){
      return trim($buffer);
  }
  ob_start("callback_trim");
  include('const.php');
  include('autoload.php');
  $db = new MySQL('system/dbsettings.php');
  $db->Connect();
  $language = new Language();
  $datatype = new DataType($_GET['datatype']);
  $serializer = new XmlSerializer();
    
  if(isset($_GET['apikey']) && $datatype->allowShare($_GET['apikey'])){ 
    if($_GET['action'] == "export"){
        $list = $datatype->getAll();
        $serializer->serialize($list,$datatype);
    }
    //At this time to unsafe
    /*else if($_GET['action'] == "import" && $_GET['url'] != ""){
          $serializer->importFromUrl($_GET['url']);
    }*/
  }
  else{
      $serializer->raiseError("1", "Access denied.");
  }
  ob_flush();
?>