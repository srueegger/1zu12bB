<?PHP
  include('const.php');
  include('autoload.php');
  if(!DEVELOPMENT){
	error_reporting(0);
        ini_set('display_errors', 0);
  }
  else{
	error_reporting(-1);
        ini_set('display_errors', 1);
  }
  session_start();
  sys::parseGetParams();
  
  //Unescape backslashes in $_POST
  array_walk_recursive($_POST, create_function('&$val', '$val = stripslashes($val);'));

  if(!isset($_GET['include'])){
    $_GET['include'] = '';
  }
  Page::Current()->ExecuteHttpHeader();
  EventManager::RaiseEvent("pre_page_load",null);
  if(file_exists(Settings::getInstance()->get("root").$_GET['include'].".htm")){
    include(Settings::getInstance()->get("root").$_GET['include'].".htm");
  }
  else{
    SkinController::displayCurrent();
  }
  Scheduler::runTasks();
?>