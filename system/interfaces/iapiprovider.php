<?php

/*
 * Interface for API providers (XML,CSV,...)
 * 
 */

/**
 *
 * @author Stefan Wienstroeer
 */
interface IApiProvider {
    
    /**
     *
     * Serializes the object and displays the code for it.
     */
    function serialize($object,DataType $datatype);
    
    /**
     *
     * Imports data objects from a given url
     */
    function importFromUrl($url);
}

?>