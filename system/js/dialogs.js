  function showIFrameDialog(title, width, height, src, reload){
    document.getElementById("iframedialog_frame").src=src;
    if(reload){
      $("#iframedialog").dialog({title: title,width: width,height: height,modal: true,resizable: false, close: function(ev, ui) { location.reload() }});
    }
    else{
      $("#iframedialog").dialog({title: title,width: width,height: height,modal: true,resizable: false});
    }
  }

  function showIFrameDialogWithCallBack(title, width, height, src, callback){
    document.getElementById("iframedialog_frame").src=src;
    $("#iframedialog").dialog({title: title,width: width,height: height,modal: true,resizable: false, close: callback});
  }