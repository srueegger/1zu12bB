<?PHP
	class UrlRewriting{
	
                /**
                 *
                 * @param string $alias
                 * @param string $parameter
                 * @return string
                 */
		public static function GetUrlByAlias($alias,$parameter = ""){
			return Settings::getValue("host").self::GetShortUrlByAlias($alias,$parameter);
		}
	
                /**
                 *
                 * @param string $alias
                 * @param string $parameter
                 * @return string 
                 */
		public static function GetShortUrlByAlias($alias,$parameter = ""){
			$res = $alias.".html";
			if(!self::hasModRewrite()){
				$res = "index.php?include=".$alias;
			}
			if($parameter != ""){
				$res .= self::GetFirstParameterSeperator().$parameter;
			}
			return $res;
		}
	
                /**
                 *
                 * @param Page $page
                 * @param string $parameter
                 * @return string
                 */
		public static function GetUrlByPage(Page $page,$parameter = ""){
			return self::getUrlByAlias($page->alias,$parameter);
		}
	
                /**
                 *
                 * @return boolean
                 */
		private static function hasModRewrite(){
			return !(getenv('HTTP_MOD_REWRITE')=='Off' || (function_exists("apache_get_modules") && !in_array("mod_rewrite",@apache_get_modules())));
		}
		
                /**
                 *
                 * @return string 
                 */
		public static function GetFirstParameterSeperator(){
			$res = "?";
			if(!self::hasModRewrite()){
				$res = "&";
			}
			return $res;
		}
	
	}
?>