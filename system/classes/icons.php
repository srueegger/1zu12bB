<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  * @link http://blog.stevieswebsite.de/2010/05/icon-klasse-cms/
  */
  class Icons{
  
  /**
   *
   * @param string $name
   * @return string 
   */
  public static function getIcon($name){
    $res = Settings::getInstance()->get("host");
    if(file_exists("../system/skins/".SkinController::getCurrentSkinName()."/icons/".strtolower($name).".png")){
      $res .= "system/skins/".SkinController::getCurrentSkinName()."/icons/".strtolower($name).".png";
    }
    else{
      $res .= "system/images/icons/".Settings::getInstance()->get("iconset")."/".strtolower($name).".png";
    }
    return $res;
  }

}
?>