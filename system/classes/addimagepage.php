<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  */
  class AddImagePage extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    public function __construct(Page$page){
      $this->page = $page;
    }
  
    public function display(){
      $template = new Template();
      $template->load("message");
      if(ImageServer::insert($_POST['path'],$_POST['name'],$_POST['description'])){
        $template->assign_var("MESSAGE",Language::DirectTranslateHtml("IMAGE_ADDED"));
		$redirect = UrlRewriting::GetUrlByAlias("admin/home","dir=".urlencode($_SESSION['dir']));
		if(isset($_POST['referrer'])){
			$redirect = $_POST['referrer'];
		}
        echo "<script type='text/javascript'>setTimeout('window.location.href= \'".$redirect."\'', 1000)</script>";
      }
      else{
        $template->assign_var("MESSAGE",Language::DirectTranslateHtml("IMAGE_NOT_ADDED"));
      }
      $template->output();
    }

    function getHeader(){
    }
    
    /**
     *
     * @return string 
     */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"&Auml;ndern\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
}
?>