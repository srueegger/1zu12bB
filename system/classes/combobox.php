<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  */
  class combobox extends Control{
    public $type       = "";
    public $fillSelect = "";
    public $onChange   = "";
    public $style      = "";
    public $id         = "";

    /**
     *
     * @return string 
     */
    public function getCode(){
      $res =  "<select name=\"".$this->name."\" style=\"".$this->style."\"";
      if($this->onChange != ""){
        $res .= " onchange=\"".$this->onChange."\"";
      }
      if($this->id != ""){
        $res .= " id=\"".$this->id."\"";
      }
      $res .= ">";
      $items = DataBase::Current()->ReadRows($this->fillSelect);
      foreach($items as $item){
        $res .= "<option value=\"".htmlentities($item->value)."\"";
        if($this->value == $item->value){
          $res .= " selected=\"true\"";
        }
        $res .= ">".htmlentities($item->label)."</option>";
      }

      $res .= "</select>";
     return $res;
    }

  }
?>