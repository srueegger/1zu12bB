<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  */
  class Action {

    private $id          = -1;
    private $icon        = "";
    private $destination = "";
    private $label       = "";

    /**
     *
     * @return int id of the action
     */
    public function getID(){
      return $this->id;
    }

    public function setID($value){
      $this->id = $value;
    }
    
    /**
     *
     * @return string 
     */
    public function getIcon(){ 
      return $this->icon;
    }

    public function setIcon($value){
      $this->icon = $value;
    }

    /**
     *
     * @param array $params
     * @return string 
     */
    public function getDestination($params){
      $res = $this->destination;
      foreach($params as $key=>$value){
        $res = str_replace("{VAR:".strtoupper($key)."}",$value, $res);
      }
      $res = str_replace("{VAR:ROOT}",Settings::getInstance()->get("root"), $res);
      $res = str_replace("{VAR:HOST}",Settings::getInstance()->get("host"), $res);

      return $res;
    }

    public function setDestination($value){
      $this->destination = $value;
    }
    
    /**
     *
     * @return string 
     */
    public function getLabel(){
      return $this->label;
    }

    public function setLabel($value){
      $this->label = $value;
    }

    /**
     *
     * @param array $params
     * @return string
     */
    public function getCode($params){
      return "<a href=\"".$this->getDestination($params)."\" title=\"".htmlentities($this->label)."\"><img src=\"".Icons::getIcon($this->icon)."\" /></a>";
    }

  }
?>