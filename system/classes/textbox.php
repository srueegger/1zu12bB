<?PHP
  class textbox extends Control{
    
    /**
     *
     * @return string
     */
    public function getCode(){
      return "<input name=\"".htmlentities($this->name)."\" value=\"".htmlentities($this->value)."\" />";
    }

  }
?>