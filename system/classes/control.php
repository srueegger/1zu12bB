<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  */
  abstract class Control{
    public $name  = "";
    public $value = "";

    /**
     *
     * Displays the code of the control
     */
    public function display(){
        echo $this->getCode();
    }
    
    /**
     *
     * Displays the code of the control
     */
    public abstract function getCode();
  }
?>