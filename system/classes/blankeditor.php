<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  * @link http://blog.stevieswebsite.de/2010/08/exception-system-cms/
  */
  class BlankEditor extends Editor{
    protected $content = "";

    /**
     *
     * @param Page $page 
     */
    public function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      echo $this->content;
    }

    public function setContent($content){
      $this->content = $content;
    }

    function getHeader(){
    }
    
    public function getEditableCode(){
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }
    
    function writeContent($content){
    }
    
    function getBreadcrumb(){
    }
  
  }
?>