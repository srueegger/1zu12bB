<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  */
  class ContentLionException extends Exception{
    protected $errorPage = null;

    /**
     *
     * @return Page 
     */
    public function getErrorPage(){
      return $this->errorPage;
    }

    public function setErrorPage(Page $page){
      $this->errorPage = $page;
    }
  }
?>