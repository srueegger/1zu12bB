<?PHP
  /**
   * Exception, if access is denied
   * @package ContentLion-Core
   * @author Stefan Wienströer
   * @link http://blog.stevieswebsite.de/2010/08/exception-system-cms/
   */
  class AccessDeniedException extends ContentLionException{

    /**
     * Exception, if access is denied
     * @param string $message message for the user
     * @param int $code errorcode for automatic handling
     */
    public function __construct($message, $code = 0) {
      parent::__construct($message, $code);
      $page = new Page();
      $page->loadPropertiesById(Settings::getInstance()->get("accessdenied"));
      parent::setErrorPage($page);
    }

  }
?>