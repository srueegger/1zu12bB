<?PHP
  class DataTypeValidator{
    private $id           = -1;
    private $dataTypeID   = -1;
    private $dataType     = null;
    private $select       = "";
    private $message      = "";
    private $finalMessage = "";

    /**
     *
     * @param DataType $type
     * @return DataTypeValidator 
     */
    public static function getByDataType(DataType $type){
      $res = array();
      $dataTypeID = DataBase::Current()->EscapeString($type->getID());
      $rows = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}datatype_validator WHERE dataType = '".$dataTypeID."'");
      foreach($rows as $row){
        $validator             = new DataTypeValidator();
        $validator->id         = $row->id;
        $validator->dataTypeID = $row->dataType;
        $validator->dataType   = $type;
        $validator->select     = $row->select;
        $validator->message    = $row->message;
        $res[] = $validator;
      }
      return $res;
    }

    /**
     *
     * @param array $params
     * @return boolean 
     */
    public function validate($params){
      $select = $this->select;
      foreach($params as $key=>$value){
        $select = str_ireplace("{VAR:".strtoupper($key)."}",DataBase::Current()->EscapeString($value),$select);
      }
      $res = DataBase::Current()->ReadField($select) > 0;
      if(!$res){
        $message = $this->message;
        foreach($params as $key=>$value){
          $message = str_ireplace("{VAR:".strtoupper($key)."}",DataBase::Current()->EscapeString($value),$message);
        }
        $this->finalMessage=$message;
      }
      return $res;
    }

    /**
     *
     * @return string 
     */
    public function getErrorMessage(){
      return $this->finalMessage;
    }

  }
?>