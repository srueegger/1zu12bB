<?PHP
  abstract class View{
    public $name       = "";
    public $fillSelect = "";

    public function display(){
        echo $this->getCode();
    }

    public abstract function getCode();
  }
?>