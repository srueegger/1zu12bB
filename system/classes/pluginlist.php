<?PHP
  class PluginList{
    public $plugins;
    
    /**
     *
     * @param PluginInfo $pluginInfo 
     */
    public function Add(PluginInfo $pluginInfo){
      $this->plugins[] = $pluginInfo;
    }
    
    public function loadAll(){
      $pluginpath = Settings::getInstance()->get("root")."system/plugins";
      $oDir = openDir($pluginpath);
      while($item = readDir($oDir)){
        $this->AddInfo($item);
      }

      closeDir($oDir);
    }
    
    private function AddInfo($plugin_name){
      $pluginpath = Settings::getInstance()->get("root")."system/plugins";
      if(is_dir($pluginpath."/".$plugin_name)){
        if(file_exists($pluginpath."/".$plugin_name."/info.php")){
          include($pluginpath."/".$plugin_name."/info.php");
        }
      }
    }
    
    public static function GetInfo($plugin_name){
      $list = new PluginList();
      $list->AddInfo($plugin_name);
      if(isset($list->plugins[0])){
        return $list->plugins[0];
      }
      return null;
    }
  }
?>