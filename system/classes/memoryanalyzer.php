<?PHP
class MemoryAnalyzer{
  public static $lastMemory = 0;

  /**
   *
   * @param string $file
   * @param string $pos 
   */
  public static function log($file,$pos){
    if(self::$lastMemory != 0){
      $diff = memory_get_usage() - self::$lastMemory;
      echo "<!-- Memory: ".$diff." - ".$file.":".$pos." -->\n";
    }
    self::$lastMemory = memory_get_usage();
  }
}
?>