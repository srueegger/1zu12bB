<?PHP
  class menueselector extends Control{
    public $style = '';
    
    /**
     *
     * @return string 
     */
    public function getCode(){
      $res = "<select name=\"".$this->name."\" style=\"".$this->style."\">";
      foreach(sys::getAllMenues() as $menue){
        if($menue->id == $this->value){
          $res .= "<option value=\"".$menue->id."\" selected=\"1\">".$menue->name."</option>";
        }
        else{
          $res .= "<option value=\"".$menue->id."\">".$menue->name."</option>";
        }
      }
      $res .= "</select>";
       return $res;
    }

  }
?>