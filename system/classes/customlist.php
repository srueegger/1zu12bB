<?PHP
  class CustomList extends View{
    public $template    = "";
    public $showButtons = true;
    public $paddingLeft = -1;
    public $escape      = true;

    /**
     *
     * @return string 
     */
    public function getCode(){
      $outerTemplate = new Template();
      $outerTemplate->load("list");
      $outerTemplate->assign_var("NAME",$this->name);
      $outerTemplate->assign_var("STYLE",$this->getListStyle());
      if($rows = DataBase::Current()->ReadRows($this->fillSelect)){
        foreach($rows as $row){
          $index = $outerTemplate->add_loop_item("ITEMS");
          $innerTemplate = new Template();
          $innerTemplate->load($this->template);
          $vars = get_object_vars($row);
          foreach($vars as $key=>$value){
            $innerTemplate->assign_var(strtoupper($key),$value);
          }
          $outerTemplate->assign_loop_var("ITEMS",$index,"ITEM",$innerTemplate->getCode($this->escape));
        }
      }
      $res = $outerTemplate->getCode();
      return $res;
    }

     private function getListStyle(){
       $res = "";
       if(!$this->showButtons){
         $res .= "list-style-type:none;";
       }
       if($this->paddingLeft != -1){
         $res .= "padding-left:".$this->paddingLeft.";";
       }
       return $res;
     }

  }
?>