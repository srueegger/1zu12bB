<?PHP
  abstract class Editor{
    
    protected $content = null;
    public   $page     = null; 
  
    abstract function display();
    abstract function getEditableCode();
    abstract function save(Page $newPage,Page $oldPage);
    abstract function getHeader();
    abstract function __construct(Page $page);
    
    function displayEditable(){
      echo $this->getEditableCode();
    }

    function displayBreadcrumb($separator,$class,$idpraefix){
      $i = 1;
      $breadcrumb = $this->page->getBreadcrumb();
      $host = Settings::getInstance()->get("host");
      while($i <= count($breadcrumb)){
        $url = UrlRewriting::GetUrlByAlias($breadcrumb[$i-1][0]);
        echo "<a href=\"".$url."\" class=\"".$class."\" 
              id=\"".$idpraefix.$i."\">".htmlentities($breadcrumb[$i-1][1])."</a>";
        if($i < count($breadcrumb)){
          echo $separator;
        }
        $i++;
      }
    }
  
    /**
     * Function for executing Http-Header
     */
    public function ExecuteHttpHeader(){
    }
  }
?>