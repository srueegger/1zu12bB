<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  * @link http://blog.stevieswebsite.de/2010/09/caching-cms/
  */
  Class Cache{
    static function setData($area, $key, $value){
      FileCache::setData($area,$key,$value);
    }

    /**
     *
     * @param string $area
     * @param string $key
     * @return mixed cached data 
     */
    static function getData($area,$key){
      return FileCache::getData($area,$key);
    }

    static function clear($area = "",$key = ""){
      FileCache::clear($area,$key);
      SessionCache::clear($area,$key);
    }

    /**
     *
     * @param string $area
     * @param string $key
     * @return boolean
     */
    static function contains($area,$key){
      return FileCache::contains($area,$key);
    }
  }
?>