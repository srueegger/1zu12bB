<?PHP
  class WYSIWYG extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
        $template = new Template();
        $template->load($this->page->getEditorContent($this));
        echo $template->getCode();
    }

    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
        $template = new Template();
        $template->load("control_wysiwyg");
        $template->assign_var("CONTENT",$this->page->getEditorContent($this));
        $template->assign_var("HOST",Settings::getInstance()->get("host"));
        $template->assign_var("ALIAS",$this->page->alias);
        $template->assign_var("URL",  UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias));
        $template->assign_var("LANG",strtolower(Settings::getInstance()->get("language")));
        $template->assign_var("PREVIEWURL",$this->page->GetUrl());
        return $template->getCode();
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
        $this->page = $newPage;
        $this->page->setEditorContent(str_replace("\\\"","\"",$_POST['content']));
        $this->page->save();
    }
      
  }
?>