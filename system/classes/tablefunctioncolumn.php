<?PHP
  class TableFunctionColumn extends TableColumn{

    public $functionName = "";

    /**
     *
     * @return string
     */
    public function getSelect(){
      return "0";
    }

    /**
     *
     * @param array $row
     * @return string 
     */
    public function getBodyCode($row){
      $res = "<td";
      if($this->autoWidth){
        $res .= " style='width:auto'";
      }
      $function = $this->functionName;
      $res .= ">".htmlentities($function($row[$this->field]))."</td>";
      return $res;
    }

  }
?>