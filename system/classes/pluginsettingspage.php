<?PHP
  class PluginSettingsPage extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $plugins = new PluginList();
      $plugins->loadAll();
      foreach($plugins->plugins as $plugin){
        if($plugin->path == $_GET['plugin']){
          echo "<h2>".$plugin->name."</h2>";
          include(Settings::getInstance()->get("root")."system/plugins/".$plugin->path.
            "/".$plugin->configurationFile);
        }
      }
    }
    
    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      $change = htmlentities(Language::GetGlobal()->getString("CHANGE"));
      return "<input name=\"save\" type=\"submit\" value=\"".$change."\" 
                onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ;
                target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
}
?>