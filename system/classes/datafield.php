<?PHP
  class DataField{
    private $id          = -1;
    private $dataType    = null;
    private $dataTypeID  = -1;
    private $displayName = "";
    private $dataName    = "";
    private $validatorID = -1;
    private $validator   = null;

    /**
     *
     * @param DataType $type
     * @param string $name
     * @return DataField 
     */
    public static function getByFormAndName(DataType $type, $name){
      $res = new DataField();
      $typeID = DataBase::Current()->EscapeString($type->getID());
      $name = DataBase::Current()->EscapeString($name);
      
      if($field = DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}datafields WHERE dataType = '".$typeID."' AND dataName = '".$name."'")){
        $res->id          = $field->id;
        $res->dataTypeID  = $field->dataType;
        $res->dataType    = $type;
        $res->displayName = $field->displayName;
        $res->dataName    = $field->dataName;
        $res->validatorID = $field->validator;
      }

      return $res;
    }

    /**
     *
     * @param DataType $dataType
     * @return array 
     */
    public static function getByDataType(DataType $dataType){
      $res = array();
      $dataTypeID = DataBase::Current()->EscapeString($dataType->getID());
      $fields = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}datafields WHERE datatype = '".$dataTypeID."'");
      foreach($fields as $field){
        $fieldObj = new DataField();
        $fieldObj->id          = $field->id;
        $fieldObj->dataTypeID  = $field->dataType;
        $fieldObj->dataType    = $dataType;
        $fieldObj->displayName = $field->displayName;
        $fieldObj->dataName    = $field->dataName;
        $fieldObj->validatorID = $field->validator;
        $res[] = $fieldObj;
      }
      return $res;
    }

    /**
     *
     * @return string 
     */
    public function getDataName(){
      return $this->dataName;
    }

    /**
     *
     * @return DataValidator 
     */
    public function getValidator(){
      if($this->validator == null){
        $this->validator = new DataValidator($this->validatorID);
      }
      return $this->validator;
    }

    /**
     *
     * @param mixed $value
     * @return boolean 
     */
    public function validate($value){
      return $this->getValidator()->validate($value);
    }

    /**
     *
     * @return string 
     */
    public function getLastError(){
      return $this->getValidator()->getLastError();
    }
  }
?>