<?PHP
  class UserList extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $userlist = new Template();
      $userlist->load("user_list");
      $userlist->assign_var("URL",$this->page->GetUrl());

      if(isset($_POST['insert'])){
        $user = new User();
        $user->name = $_POST['name'];
        $user->role = $_POST['new_user_role'];
	$user->email = $_POST['email'];
        
        if(!$user->insert($_POST['password'])){
          $userlist->assign_var("MSG",Language::DirectTranslateHtml("USER_NOT_CREATED"));
        }
      }

      if(isset($_GET['delete'])){
        $user = new User();
        $user->id = $_GET['delete'];
        if(!$user->delete()){
          $userlist->assign_var("MSG",Language::DirectTranslateHtml("USER_NOT_DELETED"));
        }
      }
      $userlist->assign_var("MSG","");

      Cache::clear("tables","userlist");
	  
      $table = new Table();
      $id    = new TableColumn("id",Language::DirectTranslate("ID"));
      $id->autoWidth = true;
      $name  = new TableColumn("name",Language::DirectTranslate("NAME"));
      $role  = new TableColumn("role",Language::DirectTranslate("ROLE"),"IFNULL((SELECT name FROM {'dbprefix'}roles WHERE id = {'dbprefix'}user.role),'')");
      $email = new TableColumn("email",Language::DirectTranslate("EMAIL"));
      $created  = new TableColumn("create_timestamp",Language::DirectTranslate("CREATED_ON"));
      $created->autoWidth = true;
      $access  = new TableColumn("last_access_timestamp",Language::DirectTranslate("LAST_ACCESS"));
      $access->autoWidth = true;
      $table->columns->add($id);
      $table->columns->add($name);
      $table->columns->add($role);
      $table->columns->add($email);
      $table->columns->add($created);
      $table->columns->add($access);
      $table->name      = "{'dbprefix'}user";
      $table->actions   = "userlist";
      $table->orderBy   = "name";
      $table->cacheName = "userlist";

      $userlist->assign_var("TABLE",$table->getCode());

      $roles = new RoleSelector();
      $roles->hideSpecialRoles = true;
      $roles->name = "new_user_role";
      $userlist->assign_var("ROLES",$roles->getCode());

      $userlist->output();
    }

    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(Language::DirectTranslate("CHANGE"))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
}
?>