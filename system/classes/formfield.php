<?PHP
class FormField{

  public $form        = null;
  public $dataName    = "";
  public $sortIndex   = -1;
  public $insert      = true;
  public $show        = true;
  public $mandatory   = false;
  public $label       = "";
  public $preAllocate = "''";
  public $edit        = "'{VAR:VALUE}'";
  private $value      = "";
  private $dataField  = null;

  public function display(){
    echo $this->getCode();
  }

  /**
   *
   * @return string 
   */
  public function getCode(){
    $res = "";
    if($this->show){
      $res = "<label for=\"".$this->getName()."\">".htmlentities($this->label).":</label> ";
      $res .= $this->getDataField()->getValidator()->getControlCode($this->getName(),$this->value);
      $res .= "<br />";
    }
    return $res;
  }

  public function save(){
    $form        = DataBase::Current()->EscapeString($this->form->id);
    $dataName    = DataBase::Current()->EscapeString($this->dataName);
    $sortIndex   = DataBase::Current()->EscapeString($this->sortIndex);
    $label       = DataBase::Current()->EscapeString($this->label);
    $preAllocate = DataBase::Current()->EscapeString($this->preAllocate);
    $edit        = DataBase::Current()->EscapeString($this->edit);
    if($this->insert && $this->insert != "off"){
      $insert = '1';
    }
    else{
      $insert = '0';
    }
    if($this->show){
      $show = '1';
    }
    else{
      $show = '0';
    }
    if($this->mandatory){
      $mandatory = '1';
    }
    else{
      $mandatory = '0';
    }
    if($this->insert){
      $insert = '1';
    }
    else{
      $insert = '0';
    }
    return DataBase::Current()->Execute("INSERT INTO {'dbprefix'}form_fields (`form`,`dataName`, `sortIndex`, `insert`, `show`, `mandatory`,`label`,`preAllocate`,`edit`) VALUES ('".$form."', '".$dataName."', '".$sortIndex."', b'".$insert."', b'".$show."' , b'".$mandatory."', '".$label."', '".$preAllocate."', '".$edit."');");
  }

  /**
   *
   * @return string
   */
  private function getName(){
    return "form".$this->form->id."_".$this->dataName;
  }

  /**
   *
   * @param array $array 
   */
  public function setValueFromArray($array){
    if(isset($array[$this->getName()])){
      $this->value = $array[$this->getName()];
    }
  }

  /**
   *
   * @return string
   */
  public function getValue(){
    return $this->value;
  }

  /**
   *
   * @return string
   */
  public function getSqlValue(){
    return str_ireplace("{VAR:VALUE}",DataBase::Current()->EscapeString($this->value),$this->edit);
  }

  public function clear(){
    $this->value = DataBase::Current()->ReadField("SELECT ".$this->preAllocate);
  }

  /**
   *
   * @return DataField
   */
  public function getDataField(){
    if($this->dataField == null){
      $this->dataField = DataField::getByFormAndName($this->form->getDataType(),$this->dataName);
    }
    return $this->dataField;
  }

  /**
   *
   * @return boolean
   */
  public function validate(){
    return $this->getDataField()->validate($this->value);
  }

  /**
   *
   * @return string
   */
  public function getLastError(){
    return str_ireplace("{VAR:NAME}","<strong>".htmlentities($this->label)."</strong>",$this->getDataField()->getLastError());
  }

}
?>