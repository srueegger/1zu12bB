<?PHP
  class ListPage extends Editor{
    /**
     *
     * @param type $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $list = unserialize($this->page->getEditorContent($this));
      $list->fillSelect = str_replace('\\\'','\'',$list->fillSelect);
      $list->display();
    }

    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
        $template = new Template();
        $template->load("listpage");
        if($list = unserialize($this->page->getEditorContent($this))){
          $template->assign_var("SELECT",str_replace('\\\'','\'',$list->fillSelect));
          $template->assign_var("TEMPLATE",$list->template);
        }
        else{
          $template->assign_var("SELECT","");
          $template->assign_var("TEMPLATE","");
        }

        $template->assign_var("HOST",Settings::getInstance()->get("host"));
        $template->assign_var("ALIAS",$this->page->alias);
        $url = UrlRewriting::GetUrlByAlias("admin/pageedit", "site=".$this->page->alias);
        $template->assign_var("URL",$url);
        $template->assign_var("LANG",Settings::getInstance()->get("language"));
        return $template->getCode();
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
        $list = new CustomList();
        $list->template    = $_POST['template'];
        $list->fillSelect  = $_POST['select'];
        $list->showButtons = false;
        $list->paddingLeft = 0;

        $this->page = $newPage;
        $this->page->setEditorContent(serialize($list));
        $this->page->save();
    }
      
  }
?>