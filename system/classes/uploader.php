<?PHP
  class Uploader extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      if(sizeOf($_FILES) == 0){
        $this->showUploadForm();
      }
      else{
        $this->upload();
      }
    }

    protected function showUploadForm(){
      $template = new Template();
      $template->load("uploadform");
      $template->assign_var("UPLOADURL",$this->page->GetUrl());
      $template->assign_var("REFERRER",htmlentities($_SERVER['HTTP_REFERER']));
      $template->output();
    }
    
    protected function upload(){
      $template = new Template();
      $template->load("upload");
      $template->assign_var("REFERRER",$_POST['referrer']);
      $template->show_if("SHOW_MEDIALIBARY",false);
      if(!file_exists(Settings::getInstance()->get("root")."content/uploads".$_SESSION['dir'])){
        mkdir(Settings::getInstance()->get("root")."content/uploads".$_SESSION['dir']);
      }
      if(FileServer::upload(Settings::getInstance()->get("root")."content/uploads".$_SESSION['dir'],$_FILES['file'])){
        $name = $_FILES['file']['name'];
        $template->assign_var("MESSAGE",str_replace("{FILENAME}",$name,Language::DirectTranslate("FILE_UPLOADED")));
        $path_info = pathinfo(Settings::getInstance()->get("root")."content/uploads".$_SESSION['dir']."/".$name);
	    if(strtolower($path_info['extension'] == 'jpg') or
        strtolower($path_info['extension'] == 'jpeg') or
        strtolower($path_info['extension'] == 'gif') or
        strtolower($path_info['extension'] == 'png') or
        strtolower($path_info['extension'] == 'bmp')){
            $template->show_if("SHOW_MEDIALIBARY",true);
            $template->assign_var("URL",  UrlRewriting::GetUrlByAlias("admin/media/addimage"));
            $template->assign_var("FILE_PATH",Settings::getInstance()->get("host")."content/uploads".$_SESSION['dir']."/".$name);
        }
      }
      else if(FileServer::$uploadFailure != ""){
        $template->assign_var("MESSAGE",FileServer::$uploadFailure);
      }
      else{
        $template->assign_var("MESSAGE",Language::DirectTranslate("FILE_NOT_UPLOADED"));
      }
      $template->output();
    }

    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(Language::DirectTranslate("CHANGE"))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }

    /**
     *
     * @param string $separator
     * @param string $class
     * @param string $idpraefix 
     */
    public function displayBreadcrumb($separator,$class,$idpraefix){
      FolderBreadcrumb::display($this->page,$separator,$class,$idpraefix);
    }  
}
?>