<?PHP
  class TranslationTokenEditor extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    public function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $language = new Language($_GET['language']);
      if(isset($_POST['translation'])){
        $language->updateTranslation($_GET['token'],$_POST['translation']);
        Cache::clear("language");
      }
      $template = new Template();
      $template->load("translationtokeneditor");
      $template->assign_var("ORIGINAL",Language::DirectTranslate($_GET['token']));
      $template->assign_var("TRANSLATION",$language->getString($_GET['token']));
      $template->output();
    }

    /**
     *
     * @return string
     */
    function getHeader(){
      return "";
   }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(Language::DirectTranslate("CHANGE"))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }
}
?>