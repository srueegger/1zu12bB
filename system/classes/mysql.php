<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  */
  class MySQL extends DataBase{
    private $connection;
    private $queries = 0;
    
    /**
     *
     * @param string $sql
     * @return mixed
     */
    public function Execute($sql){
      $this->queries++;
      $res = $this->connection->query(str_replace("{'dbprefix'}",$this->Prefix,$sql));
      IF(DEVELOPMENT){
        echo $this->error($sql);
      }
      return $res;
    } 
    
    /**
     *
     * @return array
     */
    public function GetTables(){
      $this->queries++;
      $res =  $this->connection->query("SHOW TABLES");
      while($row = $res->fetch_row()){
        $tables[] = $row[0];
      }
      sort($tables);
      return $tables;
    }
      
    /**
     *
     * @param strong $table
     * @return array
     */
    public function GetColumns($table){
      $this->queries++;
      $mysqlres = $this->Execute("SHOW COLUMNS FROM ".$table);
      while($row = $mysqlres->fetch_object()){
        $res[] = $row;
      }
      unset($mysqlres);
      return $res;
    }
      
    /**
     *
     * @param string $sql
     * @return mixed
     */
    public function ReadField($sql){
      $this->queries++;
      $res = $this->Execute($sql);
      $row = $res->fetch_row();
      unset($res);
      if($row){
	return $row[0];
      }
      else{
	return null;
      }
    }
      
    /**
     *
     * @param string $sql
     * @param boolean $allowTranslate
     * @return mixed
     */
    public function ReadRow($sql,$allowTranslate = true){
      $this->queries++;
      $res = $this->Execute($sql);
      $res = $res->fetch_object();
      if($allowTranslate && !Language::IsLoading()){
        $res = Language::GetGlobal()->replaceLanguageTokensByObject($res);
      }
      return $res;
    }
      
    /**
     *
     * @param string $sql
     * @return array 
     */
    public function ReadRows($sql){
      $this->queries++;
      $res = array();
      $mysqlRes = $this->Execute($sql);
      while($row = $mysqlRes->fetch_object()){
        if(!Language::IsLoading()){
            $row = Language::GetGlobal()->replaceLanguageTokensByObject($row);
        }
        $res[] = $row;
      }
      unset($mysqlRes);
      return $res;
    }
    
    /**
     *
     * @return int 
     */
    public function InsertID(){
      return $this->connection->insert_id;
    }
    
    /**
     *
     * @param string $text
     * @return string
     */
    public function EscapeString($text){
	  if($this->connection){
		return $this->connection->real_escape_string($text);
	  }
	  else{
		return mysql_real_escape_string($text);
	  }
    }
      
    public function Connect(){
      $host = explode(':',$this->Host);
      
      if(sizeof($host) > 1){
	  ini_set('mysqli.default_socket', $host[1]);
      }
      
      $this->connection = @new mysqli($host[0],$this->User,$this->Password,$this->Name);
      if($this->connection->connect_errno){
	die("Cannot conntect through database.");
}
    }
      
    public function Disconnect(){
      $this->connection->close();
    }

    /**
     *
     * @return int
     */
    public function countQueries(){
      return $this->queries;
    }

    /**
     *
     * @param string $sql
     * @return string 
     */
    public function error($sql){
      $res = $this->connection->error;
      if($res){
        $res = "<strong>MySQL-Error:</strong> ".htmlentities($sql)."<br />".$res."<br />Stacktrace:<br />";
        foreach(debug_backtrace() as $trace){
            $res .= $trace['file'].":".$trace['line']."<br />";
        }
        $res .= "<br />";
      }
      return $res;
    }
  }
?>