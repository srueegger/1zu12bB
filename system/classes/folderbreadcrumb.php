<?PHP
  class FolderBreadcrumb{

    /**
     *
     * @param Page $page
     * @param string $separator
     * @param string $class
     * @param string $idpraefix 
     */
    public static function display(Page $page,$separator,$class,$idpraefix){
      $host = Settings::getInstance()->get("host");
      $i = 1;
      $breadcrumb  = $page->getBreadcrumb();
      $j = 0;
      foreach($breadcrumb as $item){
        $breadcrumb[$j][0] = UrlRewriting::GetShortUrlByAlias($breadcrumb[$j][0]);
        $j++;
      }
      $currentItem = array_pop($breadcrumb);
      $fulldir = "";
      foreach(explode("/",$_SESSION['dir']) as $dir){
        if($dir != ""){
          $url = UrlRewriting::GetShortUrlByAlias("admin/home","dir=".$fulldir."/".$dir);
          $breadcrumb[] = array($url,$dir);
          $fulldir .= "/".$dir;
        }
      }
      $breadcrumb[] = $currentItem;
      while($i <= count($breadcrumb)){
        echo "<a style='display:inline' href=\"".$host.$breadcrumb[$i-1][0]."\" class=\"".$class."\" 
              id=\"".$idpraefix.$i."\">".$breadcrumb[$i-1][1]."</a>";
        if($i < count($breadcrumb)){
          echo $separator;
        }
        $i++;
      }
    }

  }
?>