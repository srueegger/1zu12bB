<?PHP
  class iconsetselector extends Control{
    public $style = '';
    
    public function getCode(){
      $res =  "<select name=\"".$this->name."\" style=\"".$this->style."\">";
      foreach(FileServer::getFolders(Settings::getInstance()->get("root")."system/images/icons") as $iconset){
        if($this->value == $iconset){
          $res .= "<option value=\"".htmlentities($iconset)."\" selected=\"1\">".htmlentities($iconset)."</option>";
        }
        else{
          $res .= "<option value=\"".htmlentities($iconset)."\">".htmlentities($iconset)."</option>";
        }
      }
      $res .= "</select>";
      return $res;
    }
  }
?>