<?PHP
  class pageselector extends Control{
    public $style       = '';
    public $noValueText = null;
    
    /**
     *
     * @return string 
     */
    public function getCode(){
      $res =  "<select name=\"".$this->name."\" style=\"".$this->style."\">";
      if(isset($this->noValueText)){
        $res .= "<option value=\"-1\">".$this->noValueText."</option>";
      }
      $res .= $this->getOptions(-1);
      $res .= "</select>";
      return $res;
    }
    
    /**
     *
     * @param int $ownerid
     * @param int $level
     * @return string
     */
    private function getOptions($ownerid,$level = 0){
        $res = "";
        $pages = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}pages WHERE owner = '".$ownerid."' ORDER BY title");
        $prefix = "";
        for($i = 0;$i< $level;$i++){
            $prefix .= "-";
        }
        foreach($pages as $page){
          $title = $prefix.htmlentities($page->title);
          if($page->id == $this->value){
            $res .= "<option value=\"".$page->id."\" selected=\"1\">".$title."</option>";
          }
          else{
            $res .= "<option value=\"".$page->id."\">".$title."</option>";
          }
          $res .= $this->getOptions($page->id,$level + 1);
        }
        return $res;
    }
  }
?>