<?PHP
  class TableColumnList {
    private $columns = array();
    public $actions  = "";

    /**
     *
     * @param TableColumn $column 
     */
    public function add(TableColumn $column){
      $this->columns[] = $column;
    }

    /**
     *
     * @return string 
     */
    public function getSelectColumns(){
      $res = "";
      foreach($this->columns as $column){
        $res .= $column->getSelect().", ";
      }
      if($res != ""){
        $res = substr($res,0,-2);
      }
      else{
        $res = "*";
      }
      return $res;
    }

    /**
     *
     * @param array $row
     * @return string 
     */
    public function getBodyCode($row){
      $res = "<tr>";
      $vars = get_object_vars($row);
      foreach($this->columns as $column){
        $res .= $column->getBodyCode($vars);
      }
      if(strlen($this->actions) > 0){
        $actions = ActionList::get($this->actions);
        $res .="<td>".$actions->getCode($vars)."</td>";
      }
      $res .="</tr>";
      return $res;
    }

    /**
     *
     * @return string 
     */
    public function getHeaderCode(){
      $res = "<thead><tr>";
      foreach($this->columns as $column){
        $res .= $column->getHeaderCode();
      }
      if(strlen($this->actions) > 0){
        $actions = Language::DirectTranslateHtml("ACTIONS");
        $res .= "<td><strong>".$actions."</strong></td>";
      }
      $res .="</tr></thead>";
      return $res;
    }
    
    /**
     * @return int column count
     */
    public function count()
    {
        return sizeOf($this->columns);
    }

  }
?>