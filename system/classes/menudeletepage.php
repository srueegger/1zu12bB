<?PHP
  class MenuDeletePage extends Editor{
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $template = new Template();
      if(!isset($_GET['delete'])){
        $template->load("menu_delete");
        $template->assign_var("CANCELURL","javascript:history.back()");
        $template->assign_var("DELETEURL",$this->page->GetUrl("menu=".urlencode($_GET['menu'])."&delete=true"));
      }
      else{
        $template->load("message");
        if(Menu::delete(DataBase::Current()->EscapeString($_GET['menu']))){
            $template->assign_var("MESSAGE",Language::DirectTranslate("MENU_DELETED"));
        }
        else{
          $template->assign_var("MESSAGE",Language::DirectTranslate("MENU_NOT_DELETED"));
        }
      }
      $template->output();
    }

    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(Language::DirectTranslate("CHANGE"))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }

    /**
     *
     * @param string $separator
     * @param string $class
     * @param string $idpraefix 
     */
    public function displayBreadcrumb($separator,$class,$idpraefix){
      FolderBreadcrumb::display($this->page,$separator,$class,$idpraefix);
    }  
}
?>