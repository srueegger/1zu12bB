<?PHP
  class Meta{
    public $pageid      = '';
    public $description = '';
    public $keywords    = '';
    public $robots      = 'index, follow';
    
    public function load(){
      $rows = DataBase::Current()->ReadRows("SELECT name, content
                                       FROM {'dbprefix'}meta_local
                                       WHERE page = '".$this->pageid."'");
      if($rows){
        foreach($rows as $row){
          if(strtolower($row->name == 'description')){
            $this->description = $row->content;
          }
          else if(strtolower($row->name == 'keywords')){
            $this->keywords = $row->content;
          }
          else if(strtolower($row->name == 'robots')){
              $this->robots = $row->content;
          }
        }
      }
    }
    
    public function save(){
      $keywords    = DataBase::Current()->EscapeString($this->keywords);
      $description = DataBase::Current()->EscapeString($this->description);
      $robots      = DataBase::Current()->EscapeString($this->robots);
      DataBase::Current()->Execute("DELETE FROM {'dbprefix'}meta_local WHERE page = '".$this->pageid."'");
      if(trim($keywords) != ""){
        DataBase::Current()->Execute("INSERT INTO {'dbprefix'}meta_local (page,               name      , content)
                                                              VALUES('".$this->pageid."','keywords','".$keywords."')");
      }
      if(trim($description) != ""){
        DataBase::Current()->Execute("insert into {'dbprefix'}meta_local (page,               name      , content)
                                                              VALUES('".$this->pageid."','description','".$description."')");
      }
      if(trim(strtolower($robots)) != 'index, follow'){
        DataBase::Current()->Execute("insert into {'dbprefix'}meta_local (page,               name      , content)
                                                              VALUES('".$this->pageid."','robots','".$robots."')");
      }                                        
    }
    
  }
?>