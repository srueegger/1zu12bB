<?php
/**
 * Description of pluginsubmitpage
 *
 * @author Stefan
 */
class PluginSubmitPage extends Editor{

    /**
     *
     * @param Page $page 
     */
    public function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
        
      if(isset($_POST['plugin_submit']))
      {
            $to      = "support@contentlion.org";
            $subject = "Neues Plugin! ".Settings::getValue("apikey");
            $message = print_r($_POST,true);
            $headers = "From:".$_POST['email'];
            if(@mail($to, $subject, $message, $headers))
            {
                echo "<p>".Language::DirectTranslateHtml("_PLUGIN_SUBMITTED")."</p>";
            }
            else
            {
                echo "<p>".Language::DirectTranslate("_PLUGIN_NOT_SUBMITTED")."</p>";
            }
      }
      else
      {
          echo Language::DirectTranslate("SUBMIT_PLUGIN_PLEASE");
          ?>
            <form method="POST">
                <table>
                    <tr>
                        <td><?PHP echo Language::DirectTranslateHtml("COMPANY_OPTIONAL"); ?>:</td>
                        <td><input name="company" /></td>
                    </tr>
                    <tr>
                        <td><?PHP echo Language::DirectTranslateHtml("FIRSTNAME"); ?>:</td>
                        <td><input name="firstname" /></td>
                    </tr>
                    <tr>
                        <td><?PHP echo Language::DirectTranslateHtml("LASTNAME"); ?>:</td>
                        <td><input name="lastname" /></td>
                    </tr>
                    <tr>
                        <td><?PHP echo Language::DirectTranslateHtml("EMAIL_ADDRESS"); ?>:</td>
                        <td><input name="email" /></td>
                    </tr>
                    <tr>
                        <td><?PHP echo Language::DirectTranslateHtml("URL_OF_ZIP"); ?>:</td>
                        <td><input type="url" name="zip" /></td>
                    </tr>
                    <tr>
                        <td><?PHP echo Language::DirectTranslateHtml("COMMENT"); ?></td>
                        <td>
                            <textarea name="comment"></textarea>
                            
                        </td>
                    </tr>
                </table>
                <input name="plugin_submit" type="submit" value="<?PHP echo Language::DirectTranslateHtml("SUBMIT_PLUGIN"); ?>" />
            </form>

          <?PHP
      }
    }
    
    function getHeader(){
    }
    
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(Language::DirectTranslate("CHANGE"))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
    
    
}

?>
