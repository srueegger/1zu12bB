<?php
/**
 * Page for Http-Redirects
 *
 * @author Stefan
 */
class RedirectPage extends Editor{
    
    private $data = array("type" => 301, "location" => "/");
      
    /**
     *
     * @param Page $page 
     */
    public function __construct(Page $page){
      $this->page = $page;
      $content = $page->getEditorContent();
      if($content){
        $this->data = @unserialize($content);
      }
      $page->inSitemap = false;
    }
  
    public function display(){
    }
    
    public function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
        $selected_301 = "";
        $selected_302 = "";
        if($this->data['type'] == 301){
            $selected_301 = " selected=\"selected\"";
        }
        if($this->data['type'] == 302){
            $selected_302 = " selected=\"selected\"";
        }
        
        $res = "
            <table style=\"width:80%\">
                <tr>
                    <td style=\"width:100px\">".Language::DirectTranslateHtml("TYPE").":</td>
                    <td>
                        <select name=\"redirect_type\">
                            <option value=\"301\"".$selected_301.">301 - ".Language::DirectTranslateHtml("MOVED_PERMANENTLY")."</option>
                            <option value=\"302\"".$selected_302.">302 - ".Language::DirectTranslateHtml("MOVED_TEMPORARY")."</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Ziel:</td>
                    <td>
                        <input type=\"url\" name=\"redirect_location\" style=\"width:80%\" value=\"".$this->data['location']."\" />
                    </td>
                </tr>
            </table>";
      $change = htmlentities(Language::GetGlobal()->getString("CHANGE"));
      return $res."<input name=\"save\" type=\"submit\" value=\"".$change."\" target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
        $this->page = $newPage;
        $this->data['type'] = $_POST['redirect_type'];
        $this->data['location'] = $_POST['redirect_location'];
        $this->page->setEditorContent(serialize($this->data));
        $this->page->save();
    }    

  
    /**
     * Function for executing Http-Header
     */
    public function ExecuteHttpHeader(){
        $text = "Moved Permanently";
        if($this->data['type'] == 302){
            $text = "Moved Temporary";
        }
        header("HTTP/1.1 ".$this->data['type']." ".$text);
        header('Location: '.$this->data['location']);
        exit;
    }
}
?>
