<?PHP
  class ImageServer{
      
    /**
     *
     * @param string $path
     * @param string $name
     * @param string $description
     * @return mixed 
     */
    public static function insert($path,$name,$description){
      $path        = DataBase::Current()->EscapeString($path);
      $name        = DataBase::Current()->EscapeString($name);
      $description = DataBase::Current()->EscapeString($description);
      $res         = DataBase::Current()->Execute("INSERT INTO {'dbprefix'}images (path,name,description) 
                                              VALUES ('".$path."','".$name."','".$description."')");
      if($res){
        $args['path']        = $path;
        $args['name']        = $name;
        $args['description'] = $description;
        EventManager::RaiseEvent("image_registered",$args);
      }
      return $res;
    }
    
    /**
     *
     * @return array 
     */
    public static function getImages(){
      return DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}images");
    }

    /**
     *
     * @param string $path
     * @return mixed
     */
    public static function getImageData($path){
      $path = DataBase::Current()->EscapeString($path);
      return DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}images WHERE path = '".$path."'");
    }

    /**
     *
     * @param string $path
     * @return mixed 
     */
    public static function delete($path){
      $path = DataBase::Current()->EscapeString($path);
      return DataBase::Current()->Execute("DELETE FROM {'dbprefix'}images WHERE path = '".$path."'");
    }

    /**
     *
     * @param string $path
     * @return boolean
     */
    public static function contains($path){
      return self::getImageData($path) != false;;
    }

    /**
     *
     * @param string $oldPath
     * @param string $newPath
     * @return boolean
     */
    public static function move($oldPath,$newPath){
      $oldPath = DataBase::Current()->EscapeString($oldPath);
      $newPath = DataBase::Current()->EscapeString($newPath);
      return DataBase::Current()->Execute("UPDATE {'dbprefix'}images SET path = '".$newPath."' WHERE path = '".$oldPath."'");
    }
  }
?>
