<?PHP
  class SessionCache extends Cache{
      
    /**
     *
     * @param string $area
     * @param string $key
     * @param mixed $value 
     */
    public static function setData($area, $key, $value){
      if(USE_CACHING)
      {
        if(!isset($_SESSION['cache'])){
          $_SESSION['cache'] = array();
        }
        if(!isset($_SESSION['cache'][$area])){
          $_SESSION['cache'][$area] = array();
        }
        $_SESSION['cache'][$area][$key] = $value;
      }
    }

    /**
     *
     * @param string $area
     * @param string $key
     * @return mixed
     */
    public static function getData($area, $key){
      return $_SESSION['cache'][$area][$key];
    }

    /**
     *
     * @param string $area
     * @param string $key
     * @return boolean
     */
    public static function contains($area, $key){
      return USE_CACHING && isset($_SESSION['cache'][$area][$key]);
    }

    /**
     *
     * @param string $area
     * @param string $key 
     */
    public static function clear($area = "",$key = ""){
      $_SESSION['cache'] = array();
    }
  }

?>