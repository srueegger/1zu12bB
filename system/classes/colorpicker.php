<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  */
  class colorpicker extends Control{

    /**
     *
     * @global boolean $colorPickerIncludes
     * @return string 
     */
    public function getCode(){
      global $colorPickerIncludes;
      $res = "";
      if(!$colorPickerIncludes){
        $colorPickerIncludes = true;
        $res .= "<script type=\"text/javascript\" src=\"/system/jscolor/jscolor.js\"></script>";
      }
    $res .=  "<input class=\"color\" name=\"".str_replace("\"","&quot;",htmlentities($this->name))."\" value=\"".str_replace("\"","&quot;",htmlentities($this->value))."\" />";
        return $res;
    }

  }
?>