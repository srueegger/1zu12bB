<?PHP
  class TranslationPage extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $form = new Form(1);
      $form->submit();

      $table = new Table();
      $token = new TableColumn("token","Token");
      $token->autoWidth = true;
      $name  = new TableColumn("name",Language::DirectTranslate("NAME"));
      $table->columns->add($token);
      $table->columns->add($name);
      $table->name       = "{'dbprefix'}languages";
      $table->actions    = "translations";
      $table->orderBy    = "name";
      $table->display();
      
      $newTranslation = Language::DirectTranslateHtml("NEW_TRANSLATION");
      echo "<h2>".$newTranslation."</h2>";
      $form->display();
    }

    /**
     *
     * @return string
     */
    function getHeader(){
      return "";
   }
    
   /**
    *
    * @return string
    */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(Language::DirectTranslate("CHANGE"))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }
}
?>