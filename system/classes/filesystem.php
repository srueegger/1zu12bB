<?PHP
  class FileSystem{

    /**
     *
     * @param string $path 
     */
    public static function deleteDir($path){
      $dir = opendir ($path);
	  if($dir){
		  while(($entry = readdir($dir)) !== false) {
			if($entry == '.' || $entry == '..') continue;
			if(is_file ($path.'/'.$entry) || is_link ($path.'/'.$entry)) {
			  unlink($path.'/'.$entry);
			}
			else if(is_dir($path."/".$entry)){
			  self::deleteDir($path."/".$entry);
			}
		  }
		  closedir ($dir);
		  rmdir ($path);
	  }
    }

    /**
     *
     * @param string $path
     * @return boolean 
     */
    public static function deleteFile($path){
      if(file_exists($path)){
        return unlink($path);
      }
      else{
        return false;
      }
    }

  }
?>