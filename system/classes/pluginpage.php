<?PHP
  class PluginPage extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    public function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $template = new Template();
      $template->load("plugins");
      $plugins = new PluginList();
      $plugins->loadAll();
      foreach($plugins->plugins as $plugin){
        $index = $template->add_loop_item("PLUGINS");
        if(isset($_GET['activate']) && $_GET['activate'] == $plugin->path){
          $plugin->activate();
        }
        elseif(isset($_GET['deactivate']) && $_GET['deactivate'] == $plugin->path){
          $plugin->deactivate();
        }
        $template->assign_loop_var("PLUGINS",$index,"NAME",htmlentities($plugin->name));
        $template->assign_loop_var("PLUGINS",$index,"PATH",htmlentities($plugin->path));
        $template->assign_loop_var("PLUGINS",$index,"DESCRIPTION",htmlentities($plugin->getDescription()));
        $template->assign_loop_var("PLUGINS",$index,"VERSION",$plugin->version);
        $template->assign_loop_var("PLUGINS",$index,"AUTHORLINK",$plugin->authorLink);
        $template->assign_loop_var("PLUGINS",$index,"AUTHORNAME",htmlentities($plugin->authorName));
        $template->assign_loop_var("PLUGINS",$index,"LICENSE",htmlentities($plugin->license));
        $template->assign_loop_var("PLUGINS",$index,"LICENSEURL",htmlentities($plugin->licenseUrl));
        if($plugin->isActivated()){
          $myurl = UrlRewriting::GetUrlByAlias($this->page->alias,"deactivate=".urlencode($plugin->path));
          $disable = Language::DirectTranslateHtml("DISABLE");
          $template->assign_loop_var("PLUGINS",$index,"ACTIVATIONLINK","<a href=\"".$myurl."\">".$disable."</a>");
        }
        else{
          $myurl = UrlRewriting::GetUrlByAlias($this->page->alias,"activate=".urlencode($plugin->path));
          $enable = Language::DirectTranslateHtml("ENABLE");
          $template->assign_loop_var("PLUGINS",$index,"ACTIVATIONLINK","<a href=\"".$myurl."\">".$enable."</a>");
        }
      }
      $template->assign_var("HOST",Settings::getValue("host"));
      $template->assign_var("APIKEY",Settings::getValue("apikey"));
      $template->output();
    }
    
    function getHeader(){
    }
    
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(Language::DirectTranslate("CHANGE"))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
}
?>