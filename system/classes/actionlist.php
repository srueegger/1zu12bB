<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  */
  class ActionList {

    public $actions  = array();
    public $category = "";

    protected function __construct(){}

    /**
     *
     * @return string 
     */
    public function getCategory(){
      return $this->category;
    }

    public function setCategory($value){
      $this->category = $value;
    }

    /**
     *
     * @param string $category
     * @return ActionList 
     */
    public static function get($category){
      $res = new ActionList();
      $res->setCategory($category);
      $res->loadActions();
      return $res;
    }

    protected function loadActions(){
      $category = DataBase::Current()->EscapeString($this->category);
      $actions = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}actionlists WHERE category='".$category."' ORDER BY id");
      foreach($actions as $action){
        $newAction = new Action();
        $newAction->setID($action->id);
        $newAction->setIcon($action->icon);
        $newAction->setDestination($action->destination);
        $newAction->setLabel($action->label);
        $this->actions[] = $newAction;
      }
    }

    /**
     *
     * @param array $params
     * @return string 
     */
    public function getCode($params){
      $res = "";

      foreach($this->actions as $action){
        $res .= $action->getCode($params)." ";
      }

      return $res;
    }

  }
?>