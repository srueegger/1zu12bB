<?PHP
  class Logout extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      ?>
        <h1>Bis zum n&auml;chsten Mal!</h1>
      <?PHP
        User::Current()->logout();
      ?>
        <script type="text/javascript">
          <!--
            setTimeout("self.location.href='<?PHP echo Settings::getInstance()->get("host"); ?>'",750);
          //-->
        </script>
      <?PHP
    }
    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(DataBase::Current()->getString("CHANGE"))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
}
?>