<?PHP
  class Register extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
        ?>
            <h2><?PHP echo Language::DirectTranslateHtml("SIGN_UP"); ?></h2>
        <?PHP
        if(User::Current()->isGuest()){
            ?>
            <form action="<?PHP echo $_SERVER['REQUEST_URI']; ?>" method="POST">
                <table>
                    <tr>
                        <td><?PHP Language::DirectTranslateHtml("USERNAME"); ?>:</td>
                        <td><input name="name" /></td>
                    </tr>
                    <tr>
                        <td><?PHP Language::DirectTranslateHtml("EMAIL"); ?>:</td>
                        <td> <input name="email" /></td>
                    </tr>
                </table>
                <input type="submit" value="<?PHP echo Language::DirectTranslateHtml("SIGN_UP"); ?>" />
            </form>
            <?PHP
        }
    }
    
    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(Language::DirectTranslate("CHANGE"))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
}
?>