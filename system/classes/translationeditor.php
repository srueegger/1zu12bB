<?PHP
  class TranslationEditor extends Editor{

    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $form = new Form(2);
      $form->submit();

      $GLOBALS['editinglanguage'] = new Language($_GET['language']);
      $table = new Table();
      $token = new TableColumn("token","Token");
      $translation = new TableFunctionColumn("token",Language::DirectTranslate("TRANSLATION"));
      $translation->functionName = "TranslationEditor_GetString";
      $translation->autoWidth = true;
      $languagetoken = new TableColumn("'".DataBase::Current()->EscapeString($_GET['language'])."' as language","Language");
      $languagetoken->value   = $_GET['language'];
      $languagetoken->visible = false;
      $table->columns->add($token);
      $table->columns->add($translation);
      $table->columns->add($languagetoken);
      $table->name    = "{'dbprefix'}language_tokens";
      $table->actions = "translation_tokens";
      $table->orderBy = "token";
      $table->size    = -1;
      $table->display();
      
      $newModule = Language::DirectTranslateHtml("NEW_MODULE");
      echo "<h2>".$newModule."</h2>";
      $form->display();
    }

   /**
    *
    * @return string
    */
   public function getHeader(){
      return "";
   }
    
   /**
    *
    * @return string
    */
    public function getEditableCode(){
      $url = UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias);
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(Language::DirectTranslate("CHANGE"))."\" onclick=\"form.action='".$url."' ; target='_self' ; return true\" />";
    }
    
    public function save(Page$newPage,Page $oldPage){
    }
}

/**
 *
 * @param string $token
 * @return string
 */
function TranslationEditor_GetString($token){
     return $GLOBALS['editinglanguage']->getString($token);
}
?>