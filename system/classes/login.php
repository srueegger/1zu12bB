<?PHP
  class Login extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
        ?>
            <h2>Login</h2>
        <?PHP
        if(isset($_POST['name']) && $_POST['name']){
            if(User::Current()->login($_POST['name'],$_POST['password'])){
                echo Language::DirectTranslate("HELLO")." ".User::Current()->name;
?>
<script language="JavaScript"><!--
var zeit=(new Date()).getTime(); 
var stoppZeit=zeit+4000; 
while((new Date()).getTime()<stoppZeit){}; 
window.location.href="<?PHP echo UrlRewriting::GetUrlByAlias("admin/home"); ?>";
// --></script> 
<?PHP
            }
            else{
                echo Language::DirectTranslate("LOGIN_FAILED");
            }
        }
        if(User::Current()->isGuest()){
            ?>
            <form method="POST">
                <table>
                    <tr>
                        <td><?PHP echo Language::DirectTranslate("USERNAME"); ?>:</td>
                        <td><input name="name" autofocus /></td>
                    </tr>
                    <tr>
                        <td><?PHP echo Language::DirectTranslate("PASSWORD"); ?>:</td>
                        <td> <input name="password" type="password" /></td>
                    </tr>
                </table>
                <input type="submit" value="Login" />
            </form>
            <?PHP
        }
        else{
?>
<script language="JavaScript"><!--
window.location.href="<?PHP echo UrlRewriting::GetUrlByAlias("admin/home"); ?>";
// --></script> 
<?PHP
        }
    }
    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      $change = htmlentities(Language::GetGlobal()->getString("CHANGE"));
      return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
}
?>