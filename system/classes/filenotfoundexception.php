<?PHP
  class FileNotFoundException extends ContentLionException{

    /**
     *
     * @param string $message
     * @param int $code 
     */
    public function __construct($message, $code = 0) {
      parent::__construct($message, $code);
      header("HTTP/1.0 404 Not Found");
      $page = new Page();
      $page->setEditor(new BlankEditor($page));
      $page->getEditor()->setContent("<h2>".Language::DirectTranslateHtml("PAGE_NOT_FOUND")."</h2><p>".Language::DirectTranslateHtml("PAGE_NOT_FOUND_LONG")."!</p>");
      parent::setErrorPage($page);
    }

  }
?>