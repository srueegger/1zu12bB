<?PHP
  class skinselector extends Control{
    public $type = "";
    
     /**
      *
      * @return string
      */
     public function getCode(){
      $template = new Template();
      $template->load("control_skinselector");
      $template->assign_var("NAME",$this->name);
      $template->assign_var("SELECTORNAME",$this->type);
      $template->assign_var("VALUE",$this->value);
      $template->assign_var("HOST",Settings::getValue("host"));
       $template->assign_var("CURRENTSKINNAME",SkinController::getSkinName($this->value));
      $i = 0;
      foreach(SkinController::getInstalledSkins() as $skin){
        $index = $template->add_loop_item("SKINS");
        $template->assign_loop_var("SKINS", $index, "SELECTORNAME",$this->type);
        $template->assign_loop_var("SKINS", $index, "INDEX", $i);
        $template->assign_loop_var("SKINS", $index, "SKINID", $skin->id);
        $template->assign_loop_var("SKINS", $index, "SKINNAME", $skin->name);
        $i++;
      }
      return $template->getCode();
    }

  }
?>