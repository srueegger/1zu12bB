<?PHP
  class SettingsForm{
    public $area     = "global";
    public $areaType = "global";
    public $role     = null;
    public $url      = "";
    public $template = "form_settings";
    public $dir      = "";    
    private $changed = false;

    public function display(){
      echo $this->getCode();
    }
    
    /**
     *
     * @return string
     */
    public function getCode(){
      if(isset($_POST['save'])){
        foreach($_POST as $property=>$value){
          if($property != "save" && $property != "roles"){
            $settings = Settings::getRootInstance()->specify($this->areaType,$this->area);
            if($this->dir != ""&& $this->dir != "/"){
              $settings = $settings->dir($this->dir);
            }
            $settings->set($property,$value,$this->role);
          }
        }
        Settings::forceReload();
        Cache::clear();
        Language::GetGlobal()->ClearCache();
        if(@header("Location:".str_replace("&save_settings=1","",$_SERVER['REQUEST_URI'])))
        {
            exit;
        }
        else
        {
            die("<script>window.location.href = '".str_replace("&save_settings=1","",$_SERVER['REQUEST_URI'])."';</script>");
        }
        $changed = true;
      }
      $template = new Template();
      $template->load($this->template);
      if($this->area != "global" || $this->areaType != "global"){
        $roleselector = "<select name=\"roles\" onchange=\"document.location.href='".$this->url.$this->getQuerySeperator()."areatype=".urlencode($this->areaType)."&area=".urlencode($this->area)."&role=' + this.options[this.selectedIndex].value + '&save_settings=1';\">";
      }
      else{
        $roleselector = "<select name=\"roles\" onchange=\"document.location.href='".$this->url.$this->getQuerySeperator()."role=' + this.options[this.selectedIndex].value + '&save_settings=1';\">";
      }
      $roles = DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}roles ORDER BY name");
      if($roles){
        foreach($roles as $role){
          if($this->role == $role->id){
            $roleselector .= "<option value=\"".$role->id."\" selected=\"selected\">".htmlentities($role->name)."</option>";
          }
          else{
            $roleselector .= "<option value=\"".$role->id."\">".$role->name."</option>";
          }
        }
      }
        
      $roleselector .= "</select>";
      $template->assign_var("ROLES",$roleselector);
      if($this->area != "global" || $this->areaType != "global"){
        $template->assign_var("URL",$this->url.$this->getQuerySeperator()."areatype=".urlencode($this->areaType)."&area=".urlencode($this->area)."&role=".$this->role."&save_settings=1");
      }
      else{
        $template->assign_var("URL",$this->url.$this->getQuerySeperator()."role=".$this->role."&save_settings=1");
      }
      $rows = Settings::getRootInstance()->specify($this->areaType,$this->area)->dir($this->dir)->getRows($this->role);
      if($rows){
        foreach($rows as $row){
          $index = $template->add_loop_item("SETTINGS");
          $template->assign_loop_var("SETTINGS", $index, "PROPERTY", $row['name']);
          $template->assign_loop_var("SETTINGS", $index, "DESCRIPTION",htmlentities($row['description']));
          $control        = new $row['type'];
          $control->name  = $row['name'];
          $control->value = $row['value'];
          $template->assign_loop_var("SETTINGS", $index, "CONTROL",$control->getCode());
        }
      }
      return $template->getCode();
    }

    /**
     *
     * @return string 
     */
    private function getQuerySeperator(){
      $res = "&";
      if(strpos($this->url,'?') === false){
        $res = "?";
      }
      return $res;
    }
    
    public function __destruct() {
        if($this->changed){
            Cache::clear();
            Language::GetGlobal()->ClearCache();
        };
    }
  }
?>