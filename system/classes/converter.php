<?PHP
class Converter{
  
  /**
   *
   * @param int $timestamp
   * @param string $notation
   * @return date 
   */
  function timestampToDate($timestamp,$notation){
	switch ($notation){
		case "iso8601":
			$date=date("Y-m-d",$timestamp);
			break;
		default:
			$date=date("d.M Y",$timestamp);
	}
	return $date;
  }
  
  /**
   *
   * @param int $timestamp
   * @param string $notation
   * @return date 
   */
  function timestampToTime($timestamp,$notation){
	switch ($notation){
		case "us":
			$time=date("h:i a",$timestamp);
			break;
		default:
			$time=date("H:M",$timestamp);
	}
	return $date;
  }
  
 }