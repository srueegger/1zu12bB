<?PHP
  $feedback_content = "";
  
  function send_feedback_mail($info = null){
    $GLOBALS['feedback_content'] = $info;
  }

  class FeedbackPage extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
       if(isset($_POST['submit'])){
           ob_start("send_feedback_mail");
           echo "_POST\n";
           print_r($_POST);
           if($_POST['allowphpinfo']){
               echo "\n\n\n_GET\n";
               print_r($_GET);
               echo "\n\n\n_SERVER\n";
               print_r($_SERVER);
               echo "\n\n\nphpinfo\n";
               phpinfo();
               echo "\n\n\nsettings\n";
               print_r(DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}settings"));
               echo "\n\n\nskins\n";
               print_r(DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}skins"));
               echo "\n\n\nactivated_plugins\n";
               print_r(DataBase::Current()->ReadRows("SELECT * FROM {'dbprefix'}activated_plugins"));
           }
           ob_end_clean();
           $to      = "rueegger@me.com";
           $subject = $_POST['subject'];
           $message = $GLOBALS['feedback_content'];
           $headers = "From:".$_POST['email'];
           if(@mail($to, $subject, $message, $headers))
           {
               if(strtoupper(Settings::getValue("language")) == "DE"){
                   echo "Feedback gesendet";
               }
               else{
                   echo "Feedback sent";
               }
           }
           else {
               if(strtoupper(Settings::getValue("language")) == "DE"){
                   echo "<p>Feedback konnte leider nicht gesendet werden. 
                        Schreiben sie direkt an: 
                        <a href='mailto:rueegger@me.com'>rueegger@me.com</a></p>";
               }
               else{
                   echo "<p>Sorry, we cannot sent the feedback. Please contact <a href='mailto:support@contentlion.org'>support@contentlion.org</a></p>";
               }
 
           }
       }
       if(strtoupper(Settings::getValue("language")) == "DE"){
       ?>
<p>
    Falls du bei der 1:12 Intiative aktiv mitmachen möchtest, kannst du dich hier melden:
</p>
<form method="POST">
    <table>
        <tr>
            <td>Name:</td>
            <td><input name="name" style="width:300px" /></td>
        </tr>
        <tr>
            <td>E-Mail:</td>
            <td><input type="email" name="email" style="width:300px" /></td>
        </tr>
        <tr>
            <td>Betreff:</td>
            <td><input name="subject" style="width:300px" /></td>
        </tr>
    </table>
    <h2>Deine Mitteilung:</h2>
    <textarea style="width:600px;height:350px;" name="content"></textarea><br />
    <input type="hidden" name="allowphpinfo" value="1" /><br />
    <input type="submit" name="submit" value="Absenden" />
</form>
           <?PHP
        }
        else{
       ?>
<p>
    You can help us make a better Content Manangement System!
    Just tell us all you wish about ContentLion.
</p>
<form method="POST">
    <table>
        <tr>
            <td>Name:</td>
            <td><input name="name" style="width:300px" /></td>
        </tr>
        <tr>
            <td>E-Mail:</td>
            <td><input type="email" name="email" style="width:300px" /></td>
        </tr>
        <tr>
            <td>Subject:</td>
            <td><input name="subject" style="width:300px" /></td>
        </tr>
    </table>
    <h2>Your Feedback</h2>
    <textarea style="width:600px;height:350px;" name="content"></textarea><br />
    <input type="checkbox" name="allowphpinfo" checked="checked" />
    <label for="allowphpinfo">Send my configuration</label><br />
    <input type="submit" name="submit" value="Absenden" />
</form>
           <?PHP
        }
    }
    
    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      return '<p><input name="save" type="submit" value="'.Language::DirectTranslate("change").'" onclick="form.action=\''. UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias).'\' ; target=\'_self\' ; return true" /></p>';
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
}
?>