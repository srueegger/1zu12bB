<?php

/**
 * Manages the rights for data sharing
 *
 * @author Stefan Wienströer
 */
class DataTypeShare {
    
    protected $dataType = null;
    protected $apiKey   = null;
    protected $name     = null;
    
    /**
     *
     * @return string 
     */
    public function GetName(){
        return $this->name;
    }
    
    /**
     *
     * @param string $name 
     */
    public function SetName($name){
        $this->name = $name;
    }
    
    /**
     *
     * @return string 
     */
    public function GetApiKey(){
        return $this->apiKey;
    }
    
    /**
     *
     * @param string $apikey 
     */
    public function SetApiKey($apikey){
        $this->apiKey = $apikey;
    }
    
    /**
     *
     * @param DataType $type 
     */
    public function SetDataType(DataType $type){
        $this->dataType = $type;
    }
    
    /**
    * 
    * returns all shares by a given datatype
    * @param array datatype
    * @return array the datatypesa
    */
    public static function GetByDataType(DataType $type){
        $res = array();
        
        $id = DataBase::Current()->EscapeString($type->getID());
        $rows = DataBase::Current()->ReadRows("SELECT 
                                            * 
                                          FROM 
                                            {'dbprefix'}datatype_sharing
                                          WHERE
                                            datatype = ".$id);
        foreach($rows as $row){
            $share           = new DataTypeShare();
            $share->dataType = $type;
            $share->apiKey   = $row->apikey;
            $share->name     = $row->name;
            $res[] = $share;
        }
        
        return $res;
    }
    
    /**
     *
     * @return string 
     */
    public function getUrl(){
        return Settings::getValue("host")."api.php?action=export&datatype=".$this->dataType->getID()."&apikey=".urlencode($this->GetApiKey());
    }
    
    /**
     *
     * @param DataType $type
     * @param string $apikey
     * @return DataTypeShare 
     */
    public static function GetByDataTypeAndApiKey(DataType $type,$apikey){
        $res = null;
        
        $id = DataBase::Current()->EscapeString($type->getID());
        $apikey = DataBase::Current()->EscapeString($apikey);
        $row = DataBase::Current()->ReadRow("SELECT 
                                           * 
                                        FROM 
                                           {'dbprefix'}datatype_sharing
                                        WHERE
                                           datatype = ".$id."
                                           AND apikey = '".$apikey."'");
        if($row){
            $res           = new DataTypeShare();
            $res->dataType = $type;
            $res->apiKey   = $row->apikey;
            $res->name     = $row->name;
        }
        
        return $res;
    }
    
    public function Insert(){
        $name   = DataBase::Current()->EscapeString($this->name);
        $apiKey = DataBase::Current()->EscapeString($this->apiKey);
        $typeID = DataBase::Current()->EscapeString($this->dataType->getID());
        DataBase::Current()->Execute("INSERT IGNORE INTO {'dbprefix'}datatype_sharing (datatype     ,apikey       ,name       )
                                                                          VALUES ('".$typeID."','".$apiKey."','".$name."')");
    }
}

?>
