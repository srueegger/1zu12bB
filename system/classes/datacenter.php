<?PHP
  class DataCenter extends Editor{
      
    /**
     *
     * @param Page $page 
     */
    public function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $host = Settings::getInstance()->get("host");
      echo "<div style=\"width:300px;margin-right:10px;float:left;\"><h2>".Language::DirectTranslateHtml("DATATYPES")."</h2>";
      $list = new LinkList();
      $url = UrlRewriting::GetUrlByAlias("admin/data","show=");
      $list->fillSelect = "SELECT displayName as text, CONCAT('".$url."',id) as href FROM {'dbprefix'}datatypes ORDER BY displayName";
      $list->display();
      echo "</div>";
      if(is_numeric($_GET['show'])){
        $id = DataBase::Current()->EscapeString($_GET['show']);
        $datatype = DataBase::Current()->ReadRow("SELECT * FROM {'dbprefix'}datatypes WHERE id = '".$id."' ORDER BY displayName");
        echo "<div><h2>".htmlentities($datatype->displayName)."</h2><p>".htmlentities($datatype->description)."</p><h2>".Language::DirectTranslateHtml("PROPERTIES")."</h2>";
        $table       = new Table();
        $displayName = new TableColumn("displayName","Name");
        $dataName    = new TableColumn("dataName","Datenname");
        $validator   = new TableColumn("validator_name","Inhalt","IFNULL((SELECT name FROM {'dbprefix'}data_validator WHERE id = {'dbprefix'}datafields.validator),'')");
        $table->columns->add($displayName);
        $table->columns->add($dataName);
        $table->columns->add($validator);
        $displayName->autoWidth = true;
        $validator->autoWidth = true;
        $table->condition = "dataType = '".DataBase::Current()->EscapeString($_GET['show'])."'";
        $table->name      = "{'dbprefix'}datafields";
        $table->orderBy   = "displayName";
        $table->display();
        echo "</div>";
        $this->displayEvents();
        $this->displaySharing();
      }
    }
    
    private function displaySharing(){
        $url = UrlRewriting::GetUrlByAlias("admin/data/share","datatype=".urlencode($_GET['show'])."&blank=true");
        $shares = Language::DirectTranslateHtml("SHARES");
        $newshare = Language::DirectTranslateHtml("NEW_SHARE");
        echo "<h2>".$shares."</h2><a href=\"javascript:showIFrameDialog('".$newshare."',300,200,'".$url."',true);\">".$newshare."</a>";
        $datatype = new DataType($_GET['show']);
        foreach($datatype->getShares() as $share){
            echo "<br /><br />".$share->GetName().":<br /><a href='".$share->getUrl()."'>".$share->getUrl()."</a>";
        }
    }
    
    private function displayEvents(){
        echo "<h2>".Language::DirectTranslateHtml("EVENTS")."</h2>";
        $table            = new Table();
        $name             = new TableColumn("UPPER(name)","Name");
        $table->columns->add($name);
        
        $description      = new TableColumn("CONCAT('{LANG:',UPPER(name),'_EVENTDESCRIPTION}')",Language::GetGlobal()->getString("WILL_BE_RAISED"));
        $description->autoWidth = true;
        $table->columns->add($description);
        
        $table->condition = "dataType = '".DataBase::Current()->EscapeString($_GET['show'])."'";
        $table->name      = "{'dbprefix'}datatype_events";
        $table->orderBy   = "name";
        $table->display();
    }

    public function getHeader(){
    }
    
    /**
     *
     * @return string 
     */
    public function getEditableCode(){
      $change = Language::DirectTranslateHtml("CHANGE");
      return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
}
?>