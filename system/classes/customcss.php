<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  * @link http://blog.stevieswebsite.de/2009/11/logout-customcss-cms/
  */
  class CustomCSS{
 
    
    /**
    * 
    * Displays a custom stylesheet (html <link>) for a page id
    * @param int id of a page
    */
    function printStylesheet($id){
      $path = self::getStylePath($id);
      if($path){
        echo "<link href=\"".$path."\" rel=\"stylesheet\" type=\"text/css\" />";
      }
    }
    
    /**
    * 
    * Returns the url of custom stylesheet for a page id
    * @param int id of a page
    * @return string path of the stylesheet
    */
    function getStylePath($id){
      $id = DataBase::Current()->EscapeString($id);
      return DataBase::Current()->ReadField("SELECT stylePath FROM {'dbprefix'}custom_css
                        WHERE id = '".$id."'");
    }

  }
?>