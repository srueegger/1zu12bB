<?PHP
  class TableColumn {

    public $field        = "";
    public $displayName  = "";
    public $customSelect = "";
    public $autoWidth    = false;
    public $visible      = true;
    public $value        = null;

    /**
     *
     * @param string $field
     * @param string $displayName
     * @param string $customSelect 
     */
    public function __construct($field = "",$displayName = "", $customSelect = ""){
      $this->field        = $field;
      $this->displayName  = $displayName;
      $this->customSelect = $customSelect;
    }

    /**
     *
     * @return string 
     */
    public function getSelect(){
      $res = $this->field;
      if(strlen($this->customSelect) > 0){
        $res = $this->customSelect." as ".$this->field;
      }
      return $res;
    }

    /**
     *
     * @param array $row
     * @return string 
     */
    public function getBodyCode($row){
      $res = "";
      if($this->visible){
        $res = "<td";
        if($this->autoWidth){
          $res .= " style='width:auto'";
        }
        $res .= ">".htmlentities($this->getValue($row))."</td>";
      }
      return $res;
    }

    /**
     *
     * @param array $row
     * @return string
     */
    private function getValue($row){
      $res = $this->value;
      if($res == null){
        $res = $row[$this->field];
      }
      return $res;
    }

    /**
     *
     * @return string 
     */
    public function getHeaderCode(){
      $res = "";
      if($this->visible){
        $res = "<td";
        if($this->autoWidth){
          $res .= " style='width:auto'";
        }
        $res .= ">".htmlentities($this->displayName)."</td>";
      }
      return $res;
    }

  }
?>