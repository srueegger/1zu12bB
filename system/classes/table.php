<?PHP
  class Table extends View{

    public $columns           = null;
    public $name              = "";
    public $size              = 20;
    public $condition         = "";
    public $orderBy           = "";
    public $actions           = "";
    public $cacheName         = "";
    public $noDataText        = "NO_DATA";

    public function __construct(){
      $this->columns = new TableColumnList();
    }

    /**
     *
     * @return string 
     */
    public function getCode(){
      $res = "";
      if($this->cacheName != "" && Cache::contains("tables",$this->cacheName)){
         $res = Cache::getData("tables",$this->cacheName);
      }
      else{
        $res = "<table class='datatable'>";
        $this->columns->actions = $this->actions;
        $res .= $this->columns->getHeaderCode();
  
        $res .= "<tbody>";
          
        $rows_found = false;
        
        if($rows = DataBase::Current()->ReadRows($this->getSelect())){
          foreach($rows as $row){
             $rows_found = true;
             $res .= $this->columns->getBodyCode($row);
          }
        }
        
        if(!$rows_found)
        {
            $text = Language::GetGlobal()->Translate($this->noDataText);
            $res.= "<tr><td colspan='".$this->columns->count()."'>".htmlentities($text)."</td></tr>";
        }
        
        $res .= "</tbody>";

        $res .= "</table>";

        if($this->cacheName){
          Cache::setData("tables",$this->cacheName,$res);
        }
      }
      return $res;
    }

    /**
     *
     * @return string 
     */
    public function getSelect(){
      $res = "SELECT ";
      $res .= $this->columns->getSelectColumns();
      $res .= " FROM " .$this->name;
      if(strlen($this->condition) > 0){
        $res .= " WHERE ".$this->condition;
      }
      if(strlen($this->orderBy) > 0){
        $res .= " ORDER BY ".$this->orderBy;
      }
      if($this->size > -1){
        $res .= " LIMIT 0,".$this->size;
      }
      return $res;
    }

  }
?>