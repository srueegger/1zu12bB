<?PHP
  /**
  * @package ContentLion-Core
  * @author Stefan Wienstroeer
  */
  class checkbox extends Control{
    
    /**
     *
     * @return string 
     */
    public function getCode(){
      $res = "<input type=\"checkbox\" name=\"".htmlentities($this->name)."\"";
      if($this->value){
        $res .= " checked=\"checked\"";
      }
      $res .= " />";
      return  $res;
    }

  }
?>