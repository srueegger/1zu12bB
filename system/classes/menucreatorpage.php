<?PHP
  class MenuCreatorPage extends Editor{
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $template = new Template();
      $template->load("menu_new");
      $template->show_if("NOTSUCCEEDED",isset($_POST['name']) == false);
      $template->show_if("SUCCEEDED",false);
      if($_GET['blank'] == true){
        $template->assign_var("URL",UrlRewriting::GetUrlByAlias("admin/newmenu","blank=true"));
      }
      else{
        $template->assign_var("URL", UrlRewriting::GetUrlByAlias("admin/newmenu"));
      }
      if(isset($_POST['name'])){
        $template->show_if("SUCCEEDED",true);
        $id = Menu::create($_POST['name'],$_SESSION['dir']);
        if(!$id){
          $template->load("message");
          $template->assign_var("MESSAGE",Language::DirectTranslate("MENU_NOT_CREATED"));
        }
      }
      $template->output();
    }
    
    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(Language::DirectTranslate("CHANGE"))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
  }
?>