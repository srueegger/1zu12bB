<?php
class DataSharingPage extends Editor{
    
    /**
     *
     * @param Page $page 
     */
    public function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
        if(isset($_POST['create'])){
            $share = new DataTypeShare();
            $share->SetApiKey($_POST['apikey']);
            $share->SetDataType(new DataType($_GET['datatype']));
            $share->SetName($_POST['name']);
            $share->Insert();
            echo "Die Freigabe wurde erteilt.";
        }
        else{
            echo "<form method=\"POST\">
                      <table>
                          <tr>
                              <td>Name:</td>
                              <td><input name=\"name\" /></td>
                          </tr>
                          <tr>
                              <td>API-Key:</td>
                              <td><input name=\"apikey\" /></td>
                          </tr>
                      </table>
                      <input name=\"create\" type=\"submit\" value=\"".Language::DirectTranslateHtml("CREATE")."\" />
                  </form>";
        }
    }

    public function getHeader(){
        
    }
    
    public function getEditableCode(){
        
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }    
    
    
    
}

?>
