<?PHP
  class NewFolderPage extends Editor{
    
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $contentpath = Settings::getInstance()->get("root")."content/articles/".$_SESSION['dir']."/";
      if(isset($_POST['name'])){
        if(FileServer::IsValidFoldername($_POST['name'])){
            FileServer::createFolder($contentpath,$_POST['name']);
            ?>
            <script language="JavaScript"><!--
                window.location.href="<?PHP echo UrlRewriting::GetUrlByAlias("admin/home","dir=".$_SESSION['dir']."/".$_POST['name']); ?>";
            // --></script> 
            <?PHP
        }
        else{
            echo Language::DirectTranslateHtml("INVALID_FOLDERNAME");
        }
      }
      $template = new Template();
      $template->load("new_folder");
      $template->output();
    }

    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(Language::DirectTranslate("CHANGE"))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }

    /**
     *
     * @param string $separator
     * @param string $class
     * @param string $idpraefix 
     */
    public function displayBreadcrumb($separator,$class,$idpraefix){
      FolderBreadcrumb::display($this->page,$separator,$class,$idpraefix);
    }  
}
?>