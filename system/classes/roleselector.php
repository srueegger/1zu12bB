<?PHP
  class RoleSelector extends Control{
    public $style = '';
    public $hideSpecialRoles = false;
    
    /**
     *
     * @return string 
     */
    public function getCode(){
      $res = "<select name=\"".$this->name."\" style=\"".$this->style."\">";
      $roles = Role::getAllRoles();
      if($roles){
        foreach($roles as $role){
          if(!$this->hideSpecialRoles || ($role->ID != 1 && $role->ID != 3)){
            if($this->value == $role->ID){
              $res .= "<option value=\"".$role->ID."\" selected=\"selected\">".htmlentities($role->name)."</option>";
            }
            else{
              $res .= "<option value=\"".$role->ID."\">".htmlentities($role->name)."</option>";
            }
          }
        }
      }
        
      $res .= "</select>";
      return $res;
    }

  }
?>