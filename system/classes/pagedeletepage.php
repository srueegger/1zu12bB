<?PHP
  class PageDeletePage extends Editor{
    
    /**
     *
     * @param Page $page 
     */
    function __construct(Page $page){
      $this->page = $page;
    }
  
    public function display(){
      $page = new Page();
      $page->loadProperties(DataBase::Current()->EscapeString($_GET['site']));
     
      $host = Settings::getInstance()->get("host");

      if(!isset($_GET['delete'])){
        $template = new Template();
        $template->load("site_delete");
        $template->assign_var("TITLE", $page->title);
        $home = UrlRewriting::GetUrlByAlias("admin/home");
        $template->assign_var("SITESURL", $home);
        $delete = UrlRewriting::GetUrlByAlias("admin/pagedelete","site=".urlencode($_GET['site'])."&delete=true");
        $template->assign_var("DELETEURL", $delete);
        $template->output();
      }
      else{
        if($page->delete()){
          $template = new Template();
          $template->load("message");
          $message = str_ireplace("{VAR:TITLE}", $page->title,Language::GetGlobal()->getString("PAGE_DELETED"));
          $template->assign_var("MESSAGE",$message);
          $template->output();
        }
      }
    }

    function getHeader(){
    }
    
    /**
     *
     * @return string
     */
    public function getEditableCode(){
      return "<input name=\"save\" type=\"submit\" value=\"".htmlentities(Language::DirectTranslate("CHANGE"))."\" onclick=\"form.action='".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."' ; target='_self' ; return true\" />";
    }
    
    /**
     *
     * @param Page $newPage
     * @param Page $oldPage 
     */
    public function save(Page $newPage,Page $oldPage){
    }

    /**
     *
     * @param string $separator
     * @param string $class
     * @param string $idpraefix 
     */
    public function displayBreadcrumb($separator,$class,$idpraefix){
      FolderBreadcrumb::display($this->page,$separator,$class,$idpraefix);
    }
}
?>