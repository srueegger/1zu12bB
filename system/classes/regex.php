<?PHP
	class Regex{
	
                /**
                 *
                 * @param string $pattern
                 * @param string $subject
                 * @param array $matchesarray
                 * @param int $flags 
                 */
		public static function MatchAll($pattern, $subject, &$matchesarray = array(),$flags = null){
                    $split = split("\n",$subject);
                    foreach($split as $item){
                        preg_match_all($pattern,$item,$results,$flags);
                        foreach($results as $result){
                            $matchesarray[] = $result;
                        }
                    }
                }
	
	}
?>