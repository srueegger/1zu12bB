<?PHP

  class FileCache extends Cache{
      
    /**
     *
     * @param string $area
     * @param string $key
     * @param mixed $value 
     */
    public static function setData($area, $key, $value){
      if(USE_CACHING){
        $file = self::getFileName($area, $key);
        $destination=fopen($file,"w");
        fwrite($destination,serialize($value));
        fclose($destination);
        chmod($file,0755);
      }
    }

    /**
     *
     * @param string $area
     * @param string $key
     * @return mixed
     */
    public static function getData($area, $key){
      $source=fopen(self::getFileName($area, $key),"r");
      $code = "";
      while ($a=fread($source,1024)){
        $code.= $a;
      }
      return unserialize($code);
    }

    /**
     *
     * @param string $area
     * @param string $key
     * @return boolean
     */
    public static function contains($area, $key){
      return USE_CACHING && file_exists(self::getFileName($area, $key));
    }

    /**
     *
     * @param string $area
     * @param string $key 
     */
    public static function clear($area = "",$key = ""){
      $path = self::getCacheDir($area);
      if($key == ""){
        FileSystem::deleteDir($path);
      }
      else{
        FileSystem::deleteFile($path."/".md5($key).".txt");
      }
    }
    
    /**
     *
     * @param string $area
     * @return string 
     */
    public static function getCacheDir($area){
      $dir = Settings::getInstance()->get("root")."/content/cache/".$area."/";
      if(!file_exists(Settings::getInstance()->get("root")."/content/cache/")){
        mkdir(Settings::getInstance()->get("root")."/content/cache/");
      }
      if(!file_exists($dir)){
        mkdir($dir);
      }
      return $dir;
    }

    /**
     *
     * @param string $area
     * @param string $key
     * @return string 
     */
    protected static function getFileName($area, $key){
      return self::getCacheDir($area).md5($key).".txt";
    }
  }

?>