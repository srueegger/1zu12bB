<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?PHP
		sys::includeHeader();
	?>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="content-language" content="de" />
	<meta name="author" content="Seggiani" />
	<link rel="stylesheet" media="screen,projection" type="text/css" href="<?PHP echo sys::getFullSkinPath(); ?>css/main.css" />
	<link rel="stylesheet" media="screen,projection" type="text/css" href="<?PHP echo sys::getFullSkinPath(); ?>css/skin.css" />
	<!--[if IE 6]><script src="<?PHP echo sys::getFullSkinPath(); ?>javascript/pngfix.js"></script><script>DD_belatedPNG.fix('#nav, #footer, #topstory .tag, .title');</script><![endif]--> 		
	<script type="text/javascript" src="<?PHP echo sys::getFullSkinPath(); ?>javascript/cufon-yui.js"></script>
	<script type="text/javascript" src="<?PHP echo sys::getFullSkinPath(); ?>javascript/font.font.js"></script>
	<script type="text/javascript">Cufon.replace('h1, h2, h3, h4, h5, h6, #logo', {hover:true});</script>	
</head>
<body>
	<div id="main">
		<!-- HEADER -->
		<div id="header" class="box">
			<p id="logo"><a href="./" title="<?PHP echo htmlentities(sys::getTitle()); ?>">Michela<span>Seggiani</span></a></p>
			<h3 id="slogan">konzepte, vermittlung, gender, kunst, geschichte</h3>		
		</div> <!-- /header -->
		<!-- NAVIGATION -->
		<div id="nav" class="box">
			<?PHP
				sys::displayGlobalMenu("<ul>","</ul>","<li>"," </li>","");
			?>
			<!-- SEARCH -->
			<form action="./suchen.html" method="post" id="search">
				<div class="relative">
					<div id="search-input"><input type="text" size="30" name="q" class="input-text" value="" placeholder="<?PHP echo(Language::DirectTranslate("plugin_search_placeholder")) ?>" /></div>
					<div id="search-submit"><input name="searchstart" type="image" src="<?PHP echo sys::getFullSkinPath(); ?>design/search-submit.png" value="<?PHP echo(Language::DirectTranslate("plugin_search_searchstart")) ?>" /></div>
				</div> <!-- /relative -->
			</form>
		</div> <!-- /nav -->
		<!-- TRAY -->
		<div id="tray" class="box">
			<p class="f-right">
				<a href="#"><img src="<?PHP echo sys::getFullSkinPath(); ?>design/ico-twitter.png" alt="Twitter" /></a>
				<a href="#"><img src="<?PHP echo sys::getFullSkinPath(); ?>design/ico-rss.png" alt="RSS" /></a>
			</p>
			<p class="f-left">
				<?PHP
					sys::displayBreadcrumb(" <span>/</span>","breadcrumb","bc");
				?>
			</p>
		</div> <!-- /tray -->
		<!-- COLUMNS -->
		<div id="section" class="box">
			<!-- CONTENT -->
			<div id="content">
				<div class="main-title box">
					<h1><?PHP echo htmlentities(Page::Current()->title); ?></h1>
				</div> <!-- /main-title -->
				<div class="padding">
					<?PHP
						if(isset($_POST['content']) && $_POST['content']){
							echo $_POST['content'];
						}
						else{
							sys::includeContent();
						}
					?>
				</div> <!-- /padding -->
			</div> <!-- /content -->
			<!-- SIDEBAR -->
			<div id="aside">
				<h3 class="title">Links</h3>
				<div class="padding">
					<ul class="menu">
						<li class="first current"><a href="#">Link1</a></li>
						<li><a href="#">Link2</a></li>
						<li><a href="#">Link3</a></li>
						<li><a href="#">Link4</a></li>
						<li class="last"><a href="#">Link5</a></li>
					</ul>		
				</div> <!-- /padding -->
				<!-- ABOUT -->
				<h3 class="title">About</h3>
				<div class="padding">
					<p class="box nomt">
						<img src="http://devcenter.2lounge.ch/content/uploads/m_seggiani.jpg" height="75" width="75" alt="" class="f-left" />
						Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas faucibus mollis interdum. Donec ullamcorper nulla non metus auctor fringilla. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.<br />
						<a href="#">Mehr</a>
					</p>
				</div> <!-- /padding -->
				<!-- SPONSORS -->
				<h3 class="title">Wichtiges</h3>
				<div class="padding">
					<ul class="sponsors">
						<li class="first">
							<a href="#">SP-Basel Stadt</a><br />
							Die Sozialdemokratische Partei -> blabla
						</li>
						<li>
							<a href="#">Sonstwas</a><br />
							und so weiter
						</li>
					</ul>
				</div> <!-- /padding -->
			</div> <!-- /aside -->
		</div> <!-- /section -->
		<!-- FOOTER -->
		<div id="footer" class="box">
			<p class="f-left">Copyright &copy;&nbsp;2013 <a href="#">Seggiani</a></p>
		</div> <!-- /footer -->
	</div> <!-- /main -->
	<script type="text/javascript"> Cufon.now(); </script>
	</body>
</html>
