<!doctype html>
<html lang="cs">
	<head>
		<?PHP
			sys::includeHeader();
		?>
		<meta charset="utf-8">
		<meta name="author" content="Seggiani">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="<?PHP echo sys::getFullSkinPath(); ?>css/main.css" media="screen,projection">
		<link rel="stylesheet" href="<?PHP echo sys::getFullSkinPath(); ?>css/print.css" media="print">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
		<script src="<?PHP echo sys::getFullSkinPath(); ?>js/custom.js"></script>
		<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body>
		<header>
			<div class="wrap clearFix">
				<a id="logo" href="./" title="<?PHP echo htmlentities(sys::getTitle()); ?>"><?PHP echo htmlentities(sys::getTitle()); ?></a>
				<hr> 
				
				<nav>
					<div id="nav">
						<strong>Navigation</strong>
						<?PHP
							sys::displayGlobalMenu("<ul>","</ul>","<li>"," </li>","");
						?>
					</div>
				</nav>
			</div>
		</header>
		<hr />
		<div id="content">
			<div class="wrap clearFix">
				<h2><?PHP echo htmlentities(Page::Current()->title); ?></h2>  
				<div class="clearFix block">
					<?PHP
						if(isset($_POST['content']) && $_POST['content']){
							echo $_POST['content'];
						}
						else
						{
							sys::includeContent();
						}
					?>
				</div>
			</div>
		</div> <!-- / #content -->	  
		
		<hr class="noPrint" />
		<hr />
		<footer>
			<ul class="wrap clearFix cols">
				<li class="clearFix first">
					<h3>Über mich</h3>
					<p>Vestibulum id ligula porta felis euismod semper. Etiam porta sem malesuada magna mollis euismod. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam.

Maecenas sed diam eget risus varius blandit sit amet non magna. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
				</li>
				<li class="clearFix middle">
					<h3>Newsletter</h3>
					<form>
						<input type="text" class="text">
						<button class="button alt">Anmelden</button>
					</form>
				</li>
				<li class="clearFix last">
					<h3>Follow us</h3>
					<p class="socials">
						<a href="./" class="fol-twit"><i></i>Twitter</a>
						<a href="./" class="fol-face"><i></i>Facebook</a>
						<a href="./" class="fol-gplus"><i></i>Google+</a>
						<a href="./" class="fol-link"><i></i>LinkedIn</a>
						<a href="./" class="fol-tube"><i></i>YouTube</a>
						<a href="./" class="fol-rss"><i></i>RSS</a>
					</ul>
				</li>
			</ul>
			<p id="copy" class="wrap">
				&copy; 2013 <a href="./">Seggiani</a>
			</p>			 
			</ul>			
		</footer>
		
	</body>
</html>
