<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
	<head>
		<?PHP
		sys::includeHeader();
		?>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="content-language" content="de" />
		<meta name="author" lang="de" content="Samuel Rüegger; http://2lounge.ch" />
		<meta name="copyright" lang="de" content="Samuel Rüegger; http://2lounge.ch" />
		<link href="<?PHP echo sys::getFullSkinPath(); ?>screen.css" type="text/css" rel="stylesheet" media="screen,projection" />
	</head>
	<body>
		<div id="layout">
			
			
	<div id="header"> 
		<h1 id="logo"><a href="./home.html" title="<?PHP echo htmlentities(sys::getTitle()); ?>"><?PHP echo htmlentities(sys::getTitle()); ?> <span>Initiative</span></a></h1>
		<hr class="noscreen" />
		<div id="navigation">
			<?PHP
				sys::displayGlobalMenu("<ul>","</ul>","<li>","</li>","");
			?>
		</div>
		<hr class="noscreen" />
	</div>
	<div id="main">
		<h1><?PHP echo(utf8_encode(htmlentities(Page::Current()->title))); ?></h1>
		<?PHP
			if(isset($_POST["content"]) && $_POST["content"]){
				echo $_POST["content"];
			}else{
				sys::includeContent();
			}
		?>
	</div>
				</div>
				<div id="footer">
				<div id="footer-inside">
					<p id="copyright">&copy; <?PHP echo(date("Y")); ?> - <a href="http://2lounge.ch">Samuel Rüegger</a></p>
					<p id="footr">Irgendwas blabla</p>
				</div><div style="clear: both;"></div></div>
	</body>
</html>
