<?PHP
	echo("<?xml version=\"1.0\"?>");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
	<head>
		<?PHP
			sys::includeHeader();
		?>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="content-language" content="de" />
		<meta name="author" lang="de" content="Seggiani" />
		<meta name="copyright" lang="de" content="Seggiani" />
		<link rel="stylesheet" media="screen,projection" type="text/css" href="<?PHP echo sys::getFullSkinPath(); ?>css/reset.css" />
		<link rel="stylesheet" media="screen,projection" type="text/css" href="<?PHP echo sys::getFullSkinPath(); ?>css/main.css" />
		<!--[if lte IE 6]><link rel="stylesheet" media="screen,projection" type="text/css" href="<?PHP echo sys::getFullSkinPath(); ?>css/main-ie6.css" /><![endif]-->
		<link rel="stylesheet" media="screen,projection" type="text/css" href="<?PHP echo sys::getFullSkinPath(); ?>css/style.css" />
		<script src="<?PHP echo sys::getFullSkinPath(); ?>js/jquery-1.3.2.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?PHP echo sys::getFullSkinPath(); ?>js/loopedslider.js" type="text/javascript" charset="utf-8"></script>
	</head>
	<body>
		<div id="main">
			<hr class="noscreen" />
			<!-- Title + Subcategories -->
			<div id="title" class="box">
				<h1><?PHP echo htmlentities(sys::getTitle()); ?></h1>
				<?PHP
					sys::displayGlobalMenu("<p id=\"subnav\">","</p>","","<span>|</span>","");
				?>
			</div> <!-- /title -->
			<!-- Breadcrumbs -->
			<p id="breadcrumbs"><strong>Du bist hier:</strong> <?PHP sys::displayBreadcrumb(" / ","breadcrumb","bc"); ?></p>
			<!-- Columns -->
			<div class="cols-top"></div>
			<div class="cols box">
				<!-- Content -->
				<div class="content">
					<!-- Perex -->
					<div class="perex">
						<p><?php echo(htmlentities(Page::Current()->title)) ?></p>
					</div> <!-- /perex -->
					<?PHP
						if(isset($_POST['content']) && $_POST['content']){
							echo $_POST['content'];
						}
						else{
							sys::includeContent();
						}
					?>
				</div> <!-- /content -->
				<!-- Sidebar -->
				<div class="aside">
					<h2>Neuigkeiten</h2>
					<ul class="ul-news">
						<li><strong>15|08</strong> <a href="#"> Lorem ipsum dolor sit amet</a></li>
						<li><strong>15|08</strong> <a href="#"> Lorem ipsum dolor sit amet</a></li>
						<li><strong>15|08</strong> <a href="#"> Lorem ipsum dolor sit amet</a></li>
						<li><strong>15|08</strong> <a href="#"> Lorem ipsum dolor sit amet</a></li>
						<li><strong>15|08</strong> <a href="#"> Lorem ipsum dolor sit amet</a></li>
					</ul>
					<p class="t-right"><a href="#"><img src="<?PHP echo sys::getFullSkinPath(); ?>design/btn-more.gif" alt="" /></a></p>
					<h2>Newsletter</h2>
					<div class="aside-box box">
						<form action="#" method="get">
							<div class="f-left"><input type="text" size="30" name="" class="input" style="width:170px;" /></div>
							<div class="f-right"><input type="image" src="<?PHP echo sys::getFullSkinPath(); ?>design/btn-ok.gif" value="OK" /></div>
						</form>
					</div> <!-- /aside-box -->
				</div> <!-- /aside -->
			</div> <!-- /cols -->
			<div class="cols-bottom"></div>
			<hr class="noscreen" />
			<!-- Footer -->
			<div id="footer" class="box">
				<p class="f-right"><a href="#" class="footer-rss">RSS Feed</a></p>
				<p class="f-left">
					<strong>&copy;&nbsp;2013 <a href="#">Seggiani</a></strong><br />
				</p>
			</div> <!-- /footer -->
		</div> <!-- /main -->
	</body>
</html>