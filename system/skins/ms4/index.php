<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?PHP
			sys::includeHeader();
		?>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="content-language" content="de" />
		<meta name="author" content="Seggiani" />
		<link rel="stylesheet" media="screen,projection" type="text/css" href="<?PHP echo sys::getFullSkinPath(); ?>css/main.css" />
		<link rel="stylesheet" media="screen,projection" type="text/css" href="<?PHP echo sys::getFullSkinPath(); ?>css/skin.css" />
		<script type="text/javascript" src="<?PHP echo sys::getFullSkinPath(); ?>javascript/cufon-yui.js"></script>
		<script type="text/javascript" src="<?PHP echo sys::getFullSkinPath(); ?>javascript/font.font.js"></script>
		<script type="text/javascript">Cufon.replace('h1, h2, h3, h4, h5, h6, #nav', {hover:true});</script>	
		<!--[if IE 6]><script src="<?PHP echo sys::getFullSkinPath(); ?>javascript/pngfix.js"></script><script>DD_belatedPNG.fix('#nav, #twitter img, #logo img, #footer');</script><![endif]--> 		
	</head>
	<body>
	<div id="main">
		<!-- NAVIGATION -->
		<div id="nav">
			<?PHP
				sys::displayGlobalMenu("<ul>","</ul>","<li>"," </li>","");
			?>
			<!-- TWITTER -->
			<p id="twitter"><a href="#" title="Folge uns auf Twitter"><img src="<?PHP echo sys::getFullSkinPath(); ?>design/twitter.png" alt="Twitter" /></a></p>
		</div> <!-- /nav -->
		<!-- TITLE -->
		<div class="title box">
			<h1><?PHP echo htmlentities(Page::Current()->title); ?></h1>
		</div> <!-- /title -->
		<!-- CONTENT -->
		<div id="content" class="box">
			<!-- COLUMN - LEFT -->
			<div class="col-content box">
				<?PHP
					if(isset($_POST['content']) && $_POST['content']){
						echo $_POST['content'];
					}
					else{
						sys::includeContent();
					}
				?>
			</div> <!-- /col-content -->
			<!-- COLUMN - RIGHT -->
			<div class="col-aside">
				<!-- SIDEBAR MENU -->
				<h3>Weitere Links</h3>
					<?PHP
						sys::displayMenu("17","<ul class=\"menu\">","</ul>","<li>"," </li>","");
					?>
				<!-- SPONSORS -->
			</div> <!-- /col-aside -->
			<div class="fix"></div>
			<div class="gradient box">
				<div class="col-left">
					<h3>Kontakt</h3>
					<p>
						<strong>Regionales Komitee</strong><br />
						<a href="#">www.domain.ch</a><br />
						<a href="#">info@domain.ch</a><br />
						Adressse<br />
						Tel: (012) 345-6789
					</p>
				</div> <!-- /col-left -->
				<div class="col-right">
					<h3>News</h3>
					<ul>
						<li>Aug. 27 &ndash; <a href="#">Lorem ipsum dolor sit amet este, consectetur adipiscin lit ma elementum</a></li>
						<li>Aug. 26 &ndash; <a href="#">Marnare, est vitae dapibus blandit, libero a wuapuim lorem</a></li>
						<li>Aug. 18 &ndash; <a href="#">Sed hendrerit hendrerit dignissim. Aenean adipiscing enim id turpis</a></li>
						<li>Aug. 13 &ndash; <a href="#">Vivamus tincidunt, nisl quis malesuada posuere nisi lacus pellentesque</a></li>
						<li>Aug. 11 &ndash; <a href="#">Quisque vitae mi in dolor porta lobortis in id purus</a></li>
					</ul>
				</div> <!-- /col-right -->
			</div> <!-- /gradient -->
		</div> <!-- /content -->
		<div id="footer" class="box">
			<p class="f-left">Copyright &copy;&nbsp;2013 <a href="#">Regionales 1:12 Komitee Basel</a></p>
		</div> <!-- /footer -->
	</div> <!-- /main -->
	<script type="text/javascript"> Cufon.now(); </script>
	</body>
</html>
