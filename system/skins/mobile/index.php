<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN"
    "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de">
  <head>
    <?PHP
      sys::includeHeader();
    ?>
  </head>
  <body style="padding:3%">
	    <h1 style="font-size:90%"><?PHP echo sys::getTitle(); ?></h1>
        <?PHP
		  if(!$_GET['include']){
          sys::displayGlobalMenu("<ol>","</ol>","<li>"," </li>",
                                 "globalmenu");
		  }
		  else{
		    echo "<ol><li><a href=\"/\">Startseite</a></li>";
		  }
        ?>
        <?PHP
          sys::displayLocalMenu("","</ol>","<li>","</li>",
                                "localmenu");
        ?>
        <div id="content">
          <?PHP
            sys::includeContent();
          ?>
		</div>
  </body>
</html>