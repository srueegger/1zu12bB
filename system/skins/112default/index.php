<?PHP
echo "<!DOCTYPE html>";
?>
<html lang="de">
	<head>
		<?PHP
			sys::includeHeader();
		?>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="author" content="Samuel Rüegger" />
		<!-- Bootstrap core CSS -->
		<link href="<?PHP echo sys::getFullSkinPath(); ?>bootstrap.min.css" rel="stylesheet">
		<!-- Bootstrap theme -->
		<link href="<?PHP echo sys::getFullSkinPath(); ?>bootstrap-theme.min.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="<?PHP echo sys::getFullSkinPath(); ?>theme.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="<?PHP echo sys::getFullSkinPath(); ?>html5shiv.js"></script>
			<script src="<?PHP echo sys::getFullSkinPath(); ?>respond.min.js"></script>
		<![endif]-->
		<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
  _paq.push(["setCookieDomain", "*.1-12-beider-basel.ch"]);
  _paq.push(["trackPageView"]);
  _paq.push(["enableLinkTracking"]);

  (function() {
    var u=(("https:" == document.location.protocol) ? "https" : "http") + "://stats.2lounge.ch/";
    _paq.push(["setTrackerUrl", u+"piwik.php"]);
    _paq.push(["setSiteId", "6"]);
    var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
    g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Piwik Code -->
	</head>
	<body>
		<!-- Fixed navbar -->
		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="./home.html"><?PHP echo sys::getTitle(); ?></a>
				</div>
				<div class="navbar-collapse collapse">
					<?PHP
						sys::displayGlobalMenu("<ul class=\"nav navbar-nav\">","</ul>","<li>","</li>","");
					?>
				</div><!--/.nav-collapse -->
			</div>
		</div>
		<div class="container theme-showcase">
			<?PHP
				if(Page::Current()->alias == "home"){
			?>
				<!-- Main jumbotron for a primary marketing message or call to action -->
				<div class="jumbotron">
					<h1>Wettbewerb?</h1>
					<p>Mach mit bei unserem Quiz, du kannst jede Woche tolle Preise gewinnen.</p><p>Unter den Gewinnern werden jeden Sonntag folgende Bücher ausgelost: 
<ul><li>Wie Reiche denken und lenken von Ueli Mäder</li>
<li>1:12-Buch vom Denknetz und JUSO Schweiz</li>
</ul>
</p>
					<p><a href="./quiz.html" class="btn btn-danger btn-lg">Quiz starten &raquo;</a></p>
				</div>
			<?PHP
				}
			?>
			<div class="page-header">
				<h1><?PHP echo(utf8_encode(htmlentities(Page::Current()->title))); ?></h1>
			</div>
			<p>
			<?PHP
				if(isset($_POST["content"]) && $_POST["content"]){
					echo $_POST["content"];
				}else{
					sys::includeContent();
				}
			?>
			</p>
			<p>&nbsp;</p>
			<div class="row">
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Offizielle Kampagnenwebseite</h3>
						</div>
						<div class="panel-body">
							<a href="http://1-12.ch" target="_blank">Hier gelangst du zur offiziellen Kampagnenwebseite!</a>
						</div>
					</div>
				</div><!-- /.col-sm-4 -->
				<div class="col-sm-4">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">Fahne bestellen</h3>
						</div>
						<div class="panel-body">
							<a href="http://1-12.ch/fahne" target="_blank">Bestelle eine 1:12 Fahne zum aufhängen!</a>
						</div>
					</div>
				</div><!-- /.col-sm-4 -->
				<div class="col-sm-4">
					<div class="panel panel-danger">
						<div class="panel-heading">
							<h3 class="panel-title">Flyer</h3>
						</div>
						<div class="panel-body" style="text-align:center;">
							<a href="http://1-12-beider-basel.ch/content/uploads/Plakat_Regiokomitee.jpg" target="_blank"><img width="225" src="http://1-12-beider-basel.ch/content/uploads/Plakat_Regiokomitee.jpg" /></a>
						</div>
					</div>
				</div><!-- /.col-sm-4 -->
			</div>
			<p>&nbsp;</p>
			<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li>&copy; 2013 Regionales 1:12 Komitee beider Basel</li>
						</ul>
					</div><!--/.nav-collapse -->
		<script src="<?PHP echo sys::getFullSkinPath(); ?>jquery.js"></script>
		<script src="<?PHP echo sys::getFullSkinPath(); ?>bootstrap.min.js"></script>
		<script src="<?PHP echo sys::getFullSkinPath(); ?>holder.js"></script>
	</body>
</html>
