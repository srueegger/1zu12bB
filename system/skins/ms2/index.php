<!doctype html>
<html lang="cs">
	<head>
		<?PHP
			sys::includeHeader();
		?>
		<meta charset="utf-8">
		<meta name="author" content="Seggiani">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="<?PHP echo(sys::getFullSkinPath()); ?>css/main.css" media="screen,projection">
		<link rel="stylesheet" href="<?PHP echo(sys::getFullSkinPath()); ?>css/print.css" media="print">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
		<script src="js/custom.js"></script>
		<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body>
		<header>
			<div class="wrap">
				<a id="logo" href="./" title="<?PHP echo htmlentities(sys::getTitle()); ?>"><?PHP echo htmlentities(sys::getTitle()); ?></a>
				<hr /> 
				<nav>
					<div id="nav">
						<strong>Navigation</strong>
						<?PHP
							sys::displayGlobalMenu("<ul>","</ul>","<li>"," </li>","");
						?>
					</div>
				</nav>
			</div>
		</header>
		<hr />
		<div id="content">
			<div class="wrap clearFix">
				<h2><?PHP echo(strtoupper(Page::Current()->title)) ?></h2>
				<div class="clearFix">
					<?PHP
					if(isset($_POST['content']) && $_POST['content']){
						echo $_POST['content'];
					}
					else
					{
						sys::includeContent();
					}
					?>
				</div>
				<form action="./" method="post">
					<fieldset>
						<label for="email">Für den Newsletter einschreiben</label>
						<input type="text" class="text" id="email">
						<button type="submit" class="button iconLeft"><i class="email"></i> Einschreiben</button> 
					</fieldset>
				</form>
			</div>
		</div> <!-- / #content -->	  
		<hr class="noPrint" />
		<footer>
			<div class="wrap clearFix">
				<p class="floatLeft">
					Copyright &copy; 2013 <a href="./">Seggiani</a>
				</p>
				<p class="socialIcons floatRight">
					<a href="./" class="rss">RSS</a>
					<a href="./" class="facebook">Facebook</a>
					<a href="./" class="twitter">Twitter</a>
				</p>
			</div>
		</footer>
	</body>
</html>
