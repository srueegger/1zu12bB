<?PHP
  $pluginInfo = new PluginInfo();
  $pluginInfo->path              = "contentlionapi";
  $pluginInfo->name              = "ContentLion API";
  $pluginInfo->authorName        = "Stefan Wienströer";
  $pluginInfo->authorLink        = "http://www.contentlion.org";
  $pluginInfo->version           = "1.0.1";
  $pluginInfo->license           = "GPL 2";
  $pluginInfo->licenseUrl        = "http://www.gnu.org/licenses/gpl.html";
  $pluginInfo->supportedLanguages = array("de","en");
  $this->Add($pluginInfo);
?>