<?PHP
  $pluginInfo = new PluginInfo();
  $pluginInfo->path              = "settingswidget";
  $pluginInfo->name              = "Settings Widget";
  $pluginInfo->description       = "Zeigt das Settings-Formular im Dashboard an";
  $pluginInfo->authorName        = "ContentLion";
  $pluginInfo->authorLink        = "http://contentlion.org";
  $pluginInfo->version           = "1.0.2";
  $pluginInfo->license           = "GPL 2";
  $pluginInfo->licenseUrl        = "http://www.gnu.org/licenses/gpl.html";
  $this->Add($pluginInfo);
?>