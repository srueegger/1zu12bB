<?PHP
  class SettingsWidget extends WidgetBase{
    public function load(){
      $this->headline = $GLOBALS['language']->getString("SETTINGS")." (".$GLOBALS['language']->getString("FOLDER").")";
      $settings = new SettingsForm();
      $settings->role = 3;
      $settings->template = "plugin_settingswidget_widget";
      if(isset($_SESSION['dir'])){
        $settings->dir = $_SESSION['dir'];
      }
      if(isset($_GET['dir'])){
        $settings->url = UrlRewriting::GetUrlByAlias($_GET['include'],"dir=".$_GET['dir']);
      }
      else{
        $settings->url = UrlRewriting::GetUrlByAlias($_GET['include']);
      }
      if(isset($_GET['areatype'])){
        $settings->areaType = $_GET['areatype'];
      }
      if(isset($_GET['area'])){
        $settings->area = $_GET['area'];
      }
      if(isset($_GET['role'])) $settings->role = $_GET['role'];
      
      $this->content = $settings->getCode();
    }
  }
?>