<?PHP
	$tokens['plugin_description'] = 'Keeps ContentLion up to date';
	$tokens['no_updates']         = 'No Updates available.';
	$tokens['install']            = 'Install';
	$tokens['install_it']         = 'Install';
	$tokens['install_complete']   = 'Installation completed';
?>