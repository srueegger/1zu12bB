<?PHP
  $pluginInfo = new PluginInfo();
  $pluginInfo->path               = "updater";
  $pluginInfo->name               = "Updater";
  $pluginInfo->authorName         = "ContentLion";
  $pluginInfo->authorLink         = "http://contentlion.org";
  $pluginInfo->version            = "1.0.5";
  $pluginInfo->license            = "GPL 2";
  $pluginInfo->licenseUrl         = "http://www.gnu.org/licenses/gpl.html";
  $pluginInfo->supportedLanguages = array("de","en");
  $this->Add($pluginInfo);
?>