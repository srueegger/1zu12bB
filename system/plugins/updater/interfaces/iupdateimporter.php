<?PHP
  interface IUpdateImporter{
    public function getUpdates();
    public function importPath($path);
    public function importCode($code);
  }
?>