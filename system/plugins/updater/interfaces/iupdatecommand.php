<?PHP
  interface IUpdateCommand {
    public function runUpdate();
    public function __toString();
    public function load($commandString);
  }
?>