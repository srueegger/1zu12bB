<?PHP
  class Updater extends WidgetBase{
    public function load(){
	  $this->headline = "Updater";
      include_once('interfaces/iupdateimporter.php');
      include_once('interfaces/iupdatecommand.php');
      if(Cache::contains("packages","all")){
        $installedPackages = $this->content = Cache::getData("packages","all");
      }
      else{
        $installedPackages = Plugin_Updater_Package::readFromDB();
        Cache::setData("packages","all",$installedPackages);
      }
      $path = "http://connect.contentlion.org/update.php?format=xml";
      $i = 0;
	  if(is_array($installedPackages)){
		foreach($installedPackages as $package){
			$path .= "&".$package->getUpdateToken($i);
			$i++;
		}
	  }
      $importer = new Plugin_Updater_UpdateXmlImporter();
      $importer->importPath($path);
      if($importer->areUpdatesAvailible()){
        $host = Settings::getInstance()->get("host");
        foreach($importer->getUpdates() as $update){
          if(isset($_GET['update']) && $_GET['update'] == $update->getName()){
            $this->content .= "<br />".Language::DirectTranslate("install_it")." ".$update->getName()."...";
            $update->runUpdate();
            Cache::clear();
            $this->content .="<br />".htmlentities(Language::DirectTranslate("plugin_updater_install_complete"));
          }
          else{
            $this->content .= "<br />".$update->getName()." - <a href='".$host."admin/home.html?update=".$update->getName()."'>".Language::DirectTranslate("plugin_updater_install")."</a>";
          }
        }
      }
      else{
        $this->content =  Language::DirectTranslate("plugin_updater_no_updates");
      }
	}
  }
?>