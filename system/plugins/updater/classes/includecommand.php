<?PHP
  class Plugin_Updater_IncludeCommand implements IUpdateCommand{
    protected $path = "";

    public function runUpdate(){
      include(Settings::getInstance()->get('root').$this->path);
    }

    public function setPath($path){
      $this->path = $path;
    }

    public function __toString(){
      return "Include:".$this->path;
    }

    public function load($commandString){
      $this->setPath(substr($commandString,8));
      return $this;
    }
  }
?>