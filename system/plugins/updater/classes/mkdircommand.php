<?PHP
  class Plugin_Updater_MkdirCommand implements IUpdateCommand{
    protected $path = "";

    public function runUpdate(){
      $path = Settings::getInstance()->get('root')."/".$this->path;
      if(!file_exists($path)){
        mkdir($path);
      }
    }

    public function setPath($path){
      $this->path = $path;
    }

    public function __toString(){
      return "Mkdir:".$this->path;
    }

    public function load($commandString){
      $this->setPath(substr($commandString,6));
      return $this;
    }
  }
?>