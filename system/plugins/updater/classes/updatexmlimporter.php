<?PHP
  class Plugin_Updater_UpdateXmlImporter implements IUpdateImporter{
    protected $updates = array();

    public function getUpdates(){
      return $this->updates;
    }

    public function importPath($path){
      if($source=@fopen($path,"r")){
        $text = "";
        while ($a=fread($source,1024)){
          $text .= $a;
        }
        fclose($source);
        return $this->importCode($text);
      }
    }

    public function importCode($code){
      $xml = new SimpleXMLElement($code);
      $updates = array();
      foreach($xml->xpath('/contentlionupdates/update') as $update){
        $package = Plugin_Updater_UpdatePackage::get($update->path);
        $package->setName($update->name);
        $updates[] = $package;
        if(strtolower($package->getName()) == "plugin_updater"){
          $this->updates[] = $package;
        }
      }
      if(!$this->areUpdatesAvailible() && sizeof($updates) > 0){
        $this->updates = $updates;
      }
      return $this;
    }

    public function areUpdatesAvailible(){
      return sizeof($this->updates) > 0;
    }
  }
?>