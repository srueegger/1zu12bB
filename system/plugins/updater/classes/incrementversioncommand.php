<?PHP
  class Plugin_Updater_IncrementVersionCommand implements IUpdateCommand{
    protected $package = "";

    public function runUpdate(){
      $name = $GLOBALS['db']->EscapeString($this->package);
      $GLOBALS['db']->Execute("UPDATE {'dbprefix'}packages SET version = version + 1 WHERE name = '".$name."'");
    }

    public function setPackage($package){
      $this->package = trim($package);
    }

    public function __toString(){
      return "IncrementVersion:".$this->package;
    }

    public function load($commandString){
      $this->setPackage(substr($commandString,17));
      return $this;
    }
  }
?>