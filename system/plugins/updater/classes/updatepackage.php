<?PHP
  class Plugin_Updater_UpdatePackage{
    public $commandText = "";
    public $commands = array();
    public $path = "";
    public $name = "";

    public static function get($path){
      $res = new Plugin_Updater_UpdatePackage();
      $source=fopen($path,"r");
      while ($a=fread($source,1024)){
        $res->commandText .= $a;
      }
      fclose($source);
      $commands = explode('
',$res->commandText);
      foreach($commands as $line){
        $command = Plugin_Updater_CommandFactory::getCommandByCommandText($line);
        if($command != null){
          $command->load($line);
          $res->commands[] = $command;
         }
      }
      $res->path = $path;
      return $res;
    }

    public function getName(){
      return $this->name;
    }

    public function setName($name){
      $this->name = $name;
    }

    public function getPath(){
      return $this->path;
    }

    public function setPath($path){
      $this->path = $path;
    }

    public function runUpdate(){
      foreach($this->commands as $command){
        $command->runUpdate();
      }
    }

  }
?>