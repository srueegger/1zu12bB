<?PHP
  class Plugin_Updater_RemoveDirCommand implements IUpdateCommand{
    protected $path = "";

    public function runUpdate(){
      @rmdir($this->path);
    }

    public function setPath($path){
      $this->path = $path;
    }

    public function __toString(){
      return "RemoveDir:".$this->path;
    }

    public function load($commandString){
      $this->setPath(substr($commandString,10));
      return $this;
    }
  }
?>