<?PHP
  class Plugin_Updater_Package {
    protected $name;
    protected $version;

    public function setName($name){
      $this->name = $name;
    }

    public function getName(){
      return $this->name;
    }

    public function setVersion($version){
      $this->version = $version;
    }

    public function getVersion(){
      return $this->version;
    }

    public function getUpdateToken($id){
      return "package".urlencode($id)."=".urlencode($this->name)."&version".urlencode($id)."=".$this->version;
    }

    public static function readFromDB(){
      $res = array();
      $i   = 0;

      $rows = $GLOBALS['db']->ReadRows("SELECT name, version FROM {'dbprefix'}packages");
      if($rows){
        foreach($rows as $row){
          $package = new Plugin_Updater_Package();
          $package->setName($row->name);
          $package->setVersion($row->version);
          $res[] = $package;
          $i++;
        }
      }

      return $res;
    }

  }
?>