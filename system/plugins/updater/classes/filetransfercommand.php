<?PHP
  class Plugin_Updater_FileTransferCommand implements IUpdateCommand{
    protected $fromPath = "";
    protected $toPath = "";

    public function runUpdate(){
      $destination=fopen(Settings::getInstance()->get("root").$this->toPath,"w");
      $source=fopen($this->fromPath,"r");
      while ($a=fread($source,1024)){
        fwrite($destination,$a);
      }
      fclose($source);
      fclose($destination);
    }

    public function setFromPath($path){
      $this->fromPath = $path;
    }

    public function setToPath($path){
      $this->toPath = $path;
    }

    public function __toString(){
      return "FileTransfer:".$this->fromPath.";".$this->toPath;
    }

    public function load($commandString){
      $command = explode(';',substr($commandString,13));
      $this->setFromPath($command[0]);
      $this->setToPath($command[1]);
      return $this;
    }
  }
?>