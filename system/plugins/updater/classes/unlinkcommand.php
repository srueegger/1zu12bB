<?PHP
  class Plugin_Updater_UnLinkCommand implements IUpdateCommand{
    protected $path = "";

    public function runUpdate(){
      @unlink($this->path);
    }

    public function setPath($path){
      $this->path = $path;
    }

    public function __toString(){
      return "UnLink:".$this->path;
    }

    public function load($commandString){
      $this->setPath(substr($commandString,7));
      return $this;
    }
  }
?>