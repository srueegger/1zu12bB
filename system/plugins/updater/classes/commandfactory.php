<?PHP
  class Plugin_Updater_CommandFactory{

    public static function getCommandByName($name){
      $res = null;
      $name = "Plugin_Updater_".$name."Command";
      if(class_exists($name)){
        $res =  new $name();
      }
      return $res;
    }

    public static function getCommandByCommandText($text){
      $parts = explode(':',$text);
      return self::getCommandByName($parts[0]);
    }

  }
?>