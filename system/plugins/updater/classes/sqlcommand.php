<?PHP
  class Plugin_Updater_SqlCommand implements IUpdateCommand{
    protected $text = "";
    
    public function setText($text){
      $this->text = $text;
    }

    public function runUpdate(){
      $GLOBALS['db']->Execute($this->text);
    }

    public function __toString(){
      return "Sql:".$this->text;
    }

    public function load($commandString){
      $this->setText(substr($commandString,4));
      return $this;
    }
  }
?>