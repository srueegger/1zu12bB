<?PHP
  class MenuListWidget extends WidgetBase{
    public function load(){
      $this->headline = Language::DirectTranslate("plugin_menulistwidget_menus");
      if(Cache::contains("menu","widget_".$_GET['dir'])){
        $this->content = Cache::getData("menu","widget_".$_GET['dir']);
      }
      else{
        if(!isset($_GET['dir']) || substr($_GET['dir'],0,1) == '.') $_GET['dir'] = "";
        $template = new Template();
        $template->load("plugin_menulistwidget_menulist");
		$newmenuurl = UrlRewriting::GetUrlByAlias("admin/newmenu");
        $template->assign_var("NEWMENUURL",$newmenuurl);
        $menus = sys::getMenues($_GET['dir']);
        foreach($menus as $menu){
          $index = $template->add_loop_item("MENUS");
	      $template->assign_loop_var("MENUS", $index, "ID", $menu->id);
	      $template->assign_loop_var("MENUS", $index, "TITLE", $menu->name);
	      $template->assign_loop_var("MENUS", $index, "PAGES", $menu->count);
		  $editurl = UrlRewriting::GetUrlByAlias("admin/editmenu","menu=".$menu->id);
	      $template->assign_loop_var("MENUS", $index, "EDITURL", $editurl);
		  $deleteurl = UrlRewriting::GetUrlByAlias("admin/deletemenu","menu=".$menu->id);
	      $template->assign_loop_var("MENUS", $index, "DELETEURL", $deleteurl);
        }
        if(!$menus){
          $template->assign_var("NOMENUS",Language::DirectTranslate("plugin_menulistwidget_no_menus"));
        }
        else{
          $template->assign_var("NOMENUS","");
        }
        $this->content = $template->getCode();
        Cache::setData("menu","widget_".$_GET['dir'],$this->content);
      }
    }
  }
?>