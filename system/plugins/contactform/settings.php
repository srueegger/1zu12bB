<?php
	Cache::clear();
	$settings = Settings::getRootInstance()->specify("plugin","contactform");
	$plugin_contactform_mail = $settings->get("contactform_mail");
	$plugin_contactform_successmessage = $settings->get("contactform_successmessage");
	
	if(isset($_POST['mail']))
	{
		Cache::clear();
		if($plugin_contactform_mail != $_POST['mail'])
		{
			$settings->set("contactform_mail",$_POST['mail']);
		}
		
		if($plugin_contactform_successmessage != $_POST['successmessage'])
		{
			$settings->set("contactform_successmessage",$_POST['successmessage']);
		}
		
		$plugin_contactform_mail = $_POST['mail'];
		$plugin_contactform_successmessage = $_POST['successmessage'];
		Settings::forceReload();
		echo Language::DirectTranslate("plugin_contactform_plugincontactformsaved");
	}
	
	$template = new Template();
	$template->load("plugin_contactform_settings");
	$template->assign_var("mail",htmlentities($plugin_contactform_mail));
	$template->assign_var("successmessage", htmlentities($plugin_contactform_successmessage));
	$template->output();
?>