<?PHP
	$tokens['plugin_description']		= "Erm�glicht die Einbindung eines Kontaktformulars";
	$tokens['pagetypetitle']			= "Kontaktformular";
	$tokens['requirefields']			= "Alle Felder sind Pflichtfelder";
	$tokens['forminputname']			= "Name";
	$tokens['forminputmail']			= "E-Mail Adresse";
	$tokens['forminputtext']			= "Mitteilung";
	$tokens['spamcontrol']				= "Spam Kontrolle, addiere beide Zahlen";
	$tokens['forminputsubmit']			= "Daten senden";
	$tokens['plugincontactformsaved']	= "Neue Mail Adresse wurde erfolgreich gespeichert!";
	$tokens['nomailfound']				= "Sie m�ssen in Ihren Einstellungen erst ihre E-Mail Adresse eingeben. Klicken sie dazu auf diesen Text.";
	$tokens['successmessage']			= "Nachricht, die nach dem Versand angezeigt wird";
?>