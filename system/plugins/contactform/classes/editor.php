<?php

	class Plugin_Contactform_Editor extends Editor {

		function __construct(Page $page) {
			$this->page = $page;
		}

		public function display () {
		
			$template = new Template();
			$template->load("plugin_contactform_contactform");
			$template->show_if('SHOWFORM', true);
			$template->show_if('SHOWMESSAGE', false);
			if(isset($_POST["contactformsubmit"]) && $_POST["url"] == "" && $_POST["inputspamcontrol"] == $_SESSION["plugin_contactform_result"]){
				$settings = Settings::getRootInstance()->specify("plugin","contactform");
				$plugin_contactform_receiver = $settings->get("contactform_mail");
				$plugin_contactform_sendername = $_POST['inputname'];
				$plugin_contactform_sendermail = $_POST['inputmail'];
				$plugin_contactform_text = $_POST['inputtext'];
				$plugin_contactform_subject = Language::DirectTranslate("plugin_contactform_pagetypetitle")." ".Settings::getInstance()->get("title");
				mail($plugin_contactform_receiver, $plugin_contactform_subject, $plugin_contactform_text, "From: $plugin_contactform_sendername <$plugin_contactform_sendermail>");
				$_SESSION['plugin_contactform_result'] = "";
				$template->show_if('SHOWFORM', false);
				$template->show_if('SHOWMESSAGE', true);
				$template->assign_var("SUCCESSMESSAGE", $settings->get("contactform_successmessage"));
			}
			$plugin_contactform_numberone = rand(1,10);
			$plugin_contactform_numbertwo = rand(1,10);
			$_SESSION['plugin_contactform_result'] = $plugin_contactform_numberone + $plugin_contactform_numbertwo;
			$template->assign_var('FORMURL', UrlRewriting::GetUrlByAlias($this->page->alias));
			$template->assign_var('NUMBER1', $plugin_contactform_numberone);
			$template->assign_var('NUMBER2', $plugin_contactform_numbertwo);
			Cache::clear();
			echo $template->getCode();
		}

		public function getHeader () {
			return "<script type=\"text/javascript\" src=\"" . Settings::getInstance()->get("host") . "system/plugins/contactform/js/validate.js\"></script>";
		}

		public function getEditableCode() {
			Cache::clear();
			$change = htmlentities(Language::GetGlobal()->getString("CHANGE"));
			$plugin_contactform_res = "<h3>".Language::DirectTranslate("plugin_contactform_pagetypetitle")."</h3>";
			$settings = Settings::getRootInstance()->specify("plugin","contactform");
			if($settings->get("contactform_mail") == ""){
				$plugin_contactform_res .= "<p><a href=\"" . Settings::getInstance()->get("host") . "admin/pluginsettings.html?plugin=contactform\">".Language::DirectTranslate("plugin_contactform_nomailfound")."</a></p>";
			}
			$plugin_contactform_res .= "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
			return $plugin_contactform_res;
		}

		public function save(Page $newPage, Page $oldPage) {
			
		}
	}
?>