<?PHP
  class FormListWidget extends WidgetBase{
    public function load(){
      $this->headline = Language::DirectTranslate("plugin_formlistwidget_forms");
	  
	  if(isset($_POST['plugin_formlistwidget_deleteid'])){
		$form = new Form($_POST['plugin_formlistwidget_deleteid']);
		$form->Delete();
	  }

      $template = new Template();
      $template->load("plugin_formlistwidget_formlist");
      $forms = Form::getByDir($_GET['dir']);
      if($forms){
        foreach($forms as $form){
          $index = $template->add_loop_item("FORMS");
          $template->assign_loop_var("FORMS",$index,"NAME",$form->name);
          $template->assign_loop_var("FORMS",$index,"EDITURL",UrlRewriting::GetUrlByAlias("admin/formedit","form=".$form->id));
          $template->assign_loop_var("FORMS",$index,"ID",$form->id);
        }
        $template->assign_var("NOFORMS","");
      }
      else{
       $template->assign_var("NOFORMS",Language::DirectTranslate("plugin_formlistwidget_no_forms"));
      }
      $this->content = $template->getCode();
    }
  }
?>