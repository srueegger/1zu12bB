<?PHP
  $pluginInfo = new PluginInfo();
  $pluginInfo->path               = "formlistwidget";
  $pluginInfo->name               = "Formlist Widget";
  $pluginInfo->description        = "Listet die ContentLion Formulare auf";
  $pluginInfo->authorName         = "ContentLion";
  $pluginInfo->authorLink         = "http://contentlion.org";
  $pluginInfo->version            = "1.0.1";
  $pluginInfo->license            = "GPL 2";
  $pluginInfo->licenseUrl         = "http://www.gnu.org/licenses/gpl.html";
  $pluginInfo->supportedLanguages = array("de","en");
  $this->Add($pluginInfo);
?>