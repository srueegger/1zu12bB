<?PHP
  class PageListWidget extends WidgetBase{
    public function load(){
      $dir = '';
      if(isset($_GET['dir']) &&  substr($_GET['dir'],0,1) != '.'){
        $dir = $_GET['dir'];
      }

      $this->headline = Language::DirectTranslate("plugin_pagelistwidget_pages");

      if(Cache::contains("page","widget_".$dir)){
        $this->content = Cache::getData("page","widget_".$dir);
      }
      else{
        $template = new Template();
        $template->load("plugin_pagelistwidget_pagelist");
        $pages = Page::getPagesByDir($dir);
        if(substr($dir,0,1) == "/"){
          $shortdir = substr($dir,1);
        }
        else{
          $shortdir = $dir;
        }
        if($pages){
          foreach($pages as $page){
            $shortalias = substr($page->alias,strlen($shortdir));
            if(substr($shortalias,0,1) == "/"){
              $shortalias = substr($shortalias,1);
            }
            else{
              $shortalias = $shortalias;
            }
            if(!strpos($shortalias,"/")){
              $host = Settings::getInstance()->get("host");
              $index = $template->add_loop_item("PAGES");
              $template->assign_loop_var("PAGES",$index,"TITLE",$page->title);
              $template->assign_loop_var("PAGES",$index,"ALIAS",$shortalias);
			  $editurl = UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$page->alias);
              $template->assign_loop_var("PAGES",$index,"EDITURL",$editurl);
			  $deleteurl = UrlRewriting::GetUrlByAlias("admin/pagedelete","site=".$page->alias);
              $template->assign_loop_var("PAGES",$index,"DELETEURL",$deleteurl);
			  $showurl = UrlRewriting::GetUrlByAlias($page->alias);
              $template->assign_loop_var("PAGES",$index,"SHOWURL",$showurl);
            }
          }
          $template->assign_var("NOPAGES","");
        }
        else{
         $template->assign_var("NOPAGES",Language::DirectTranslate("plugin_pagelistwidget_no_pages"));
        }
        $this->content = $template->getCode();
		if(isset($_GET['dir'])){
			Cache::setData("page","widget_".$_GET['dir'],$this->content);
		}
      }
    }
  }
?>