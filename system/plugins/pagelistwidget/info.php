<?PHP
  $pluginInfo = new PluginInfo();
  $pluginInfo->path               = "pagelistwidget";
  $pluginInfo->name               = "PageList Widget";
  $pluginInfo->description        = "Listet die ContentLion Seiten auf";
  $pluginInfo->authorName         = "ContentLion";
  $pluginInfo->authorLink         = "http://contentlion.org";
  $pluginInfo->version            = "1.0.2";
  $pluginInfo->license            = "GPL 2";
  $pluginInfo->licenseUrl         = "http://www.gnu.org/licenses/gpl.html";
  $pluginInfo->supportedLanguages = array("de","en");
  $this->Add($pluginInfo);
?>