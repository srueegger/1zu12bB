<?PHP
	$tokens['PLUGIN_DESCRIPTION'] = "Erlaubt es, das Passwort zu �ndern.";
	$tokens['CHANGEPASSWORD'] = "Passwort �ndern";
	$tokens['WRONG-CONFIRM-PASSWORD'] = "Die angegebenen Passw�rter stimmen nicht �berein!";
	$tokens['OLD-PASSWORD-WRONG'] = "Das aktuelle Passwort stimmt nicht �berein!";
	$tokens['SUCCESSFUL'] = "Passwort erfolgreich ge�ndert!";
	$tokens['CURRENT-PASSWORD'] = "Aktuelles Passwort";
	$tokens['NEW-PASSWORD'] = "Neues Passwort";
	$tokens['CONFIRM-PASSWORD'] = "Passwort best�tigen";
	$tokens['TITLE'] = "Passwort �ndern";
?>