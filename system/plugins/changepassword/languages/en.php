<?PHP
	$tokens['PLUGIN_DESCRIPTION'] = "Adds a page for change your password.";
	$tokens['CHANGEPASSWORD'] = "Change Password";
	$tokens['WRONG-CONFIRM-PASSWORD'] = "Confirm Password is wrong!";
	$tokens['OLD-PASSWORD-WRONG'] = "The current password is incorrect!";
	$tokens['SUCCESSFUL'] = "Password changed succesful!";
	$tokens['CURRENT-PASSWORD'] = "Current Password";
	$tokens['NEW-PASSWORD'] = "New Password";
	$tokens['CONFIRM-PASSWORD'] = "Confirm Password";
	$tokens['TITLE'] = "Change Password";
?>