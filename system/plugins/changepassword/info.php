<?PHP
  $pluginInfo                   = new PluginInfo();
  $pluginInfo->path             = "changepassword";
  $pluginInfo->name             = "Change Password";
  $pluginInfo->authorName       = "Justin K�nzel";
  $pluginInfo->authorLink       = "mailto://JustinKuenzel@aol.com";
  $pluginInfo->version          = "1.0.1";
  $pluginInfo->license          = "GPL 3";
  $pluginInfo->licenseUrl       = "http://www.gnu.org/licenses/gpl.html";
  $pluginInfo->supportedLanguages = array("de","en");
  $this->Add($pluginInfo);
?>