<?php

/*
 * ChangePassword - Deinstallationsscript 
 * Copyright Justin K�nzel
 * 
 * Package changepassword
*/

    //Seiten-Typ l�schen
    DataBase::Current()->Execute("DELETE FROM `{'dbprefix'}pagetypes` WHERE `class` = 'Plugin_ChangePassword_Page'; ");

	$mainPage = new Page();
	$mainPage->loadProperties("admin/changepassword");
	MenuEntry::DeleteByPage($mainPage);
	$mainPage->delete();

?>