<?php

/*
 * ChangePassword - Installationsscript 
 * Copyright Justin K�nzel
 * 
 * Package changepassword
*/

Language::ClearCache();

    //Seiten-Typ anlegen
    DataBase::Current()->Execute("INSERT INTO `{'dbprefix'}pagetypes` (
    `id`, `class`, `name`
    ) VALUES (
    NULL, 'Plugin_ChangePassword_Page', '{LANG:PLUGIN_CHANGEPASSWORD_CHANGEPASSWORD}'
    ); ");
	
	$id = Page::create("admin/changepassword", "Plugin_ChangePassword_Page", "{LANG:PLUGIN_CHANGEPASSWORD_CHANGEPASSWORD}");

	$menuEntry = new MenuEntry();
	$menuEntry->menu = 4;
	$menuEntry->title = "{LANG:PLUGIN_CHANGEPASSWORD_CHANGEPASSWORD}";
	$menuEntry->href = $id;
	$menuEntry->type = 0;
	$menuEntry->save();

?>