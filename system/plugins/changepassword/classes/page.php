<?php

class Plugin_ChangePassword_Page extends Editor {

    function __construct(Page $page) {
			$this->page = $page;
		}
    
    public function display () {
    
        $template = new Template();
        $template->load("plugin_changepassword_changepassword");
        
        $template->show_if('PASSWORD_WRONG', false);
        $template->show_if('SUCCESSFUL', false);
        
        $template->show_if('OLD_PASSWORD_WRONG', false);
    
        if (isset($_REQUEST['old_password']) && !empty($_REQUEST['old_password']) && is_string($_REQUEST['old_password']) && isset($_REQUEST['new_password']) && !empty($_REQUEST['new_password']) && is_string($_REQUEST['new_password']) && isset($_REQUEST['confirm_password']) && !empty($_REQUEST['confirm_password']) && is_string($_REQUEST['confirm_password'])) {
            $old_password = DataBase::Current()->EscapeString($_REQUEST['old_password']);
            $new_password = DataBase::Current()->EscapeString($_REQUEST['new_password']);
            
            $confirm_password = DataBase::Current()->EscapeString($_REQUEST['confirm_password']);
            
            if ($new_password != $confirm_password) {
                $template->show_if('PASSWORD_WRONG', true);
            } else {
                $password = DataBase::Current()->EscapeString(md5($new_password . Settings::getInstance()->get("salt")));
                $old_password = DataBase::Current()->EscapeString(md5($old_password . Settings::getInstance()->get("salt")));
                
                $db_password = DataBase::Current()->ReadField("SELECT `password` FROM `{'dbprefix'}user` WHERE `id` = '" . User::Current()->id . "'; ");
                                                                
                if ($db_password && $db_password != null) {
                    if ($db_password != $old_password) {
                        $template->show_if('OLD_PASSWORD_WRONG', true);
                    } else {
                
                        DataBase::Current()->Execute("UPDATE `{'dbprefix'}user` SET `password` = '" . $password . "' WHERE `id` = '" . User::Current()->id . "'; ");
                        $template->show_if('SUCCESSFUL', true);
                        
                        EventManager::raiseEvent("plugin_changepassword_change", array('old_password' => $old_password, 'new_password' => $password, 'userid' => User::Current()->id));
                    
                        Cache::clear("tables","userlist");
                    
                    }
                } else {
                    //Der User ist nicht in der Datenbank aufgeführt.
                }
            }
            
        }
        
        $template->assign_var('ACTION', UrlRewriting::GetUrlByAlias($this->page->alias));
        
        echo $template->getCode();
        
    }
    
    public function getHeader () {
        return "<link rel='stylesheet' href=\"" . Settings::getInstance()->get("host") . "system/plugins/changepassword/style.css\" type=\"text/css\" media=\"all\" />";
    }
    
    public function getEditableCode() {
        $change = htmlentities(Language::GetGlobal()->getString("CHANGE"));
        return "<input name=\"save\" type=\"submit\" value=\"".$change."\" onclick=\"form.action='".UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias)."' ; target='_self' ; return true\" />";
	}
	
	public function save(Page $newPage, Page $oldPage) {}
	
}

?>