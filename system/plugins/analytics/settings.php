<?php
	Cache::clear();
	
	$template = new Template();
	$template->load("plugin_analytics_settings");
	$template->show_if("SHOWGOOGLE", false);
	$template->show_if("SHOWPIWIK", false);
	
	
	$settings = Settings::getRootInstance()->specify("plugin","analytics");
	
	if(!isset($_POST["analyticssystem"]))
	{
		$plugin_analytics_system = $settings->get("analytics_system",null,false);
	}
	elseif(isset($_POST["analyticssystem"]) && $_POST["analyticssystem"] == "google")
	{
		$plugin_analytics_system = "google";
	}
	elseif(isset($_POST["analyticssystem"]) && $_POST["analyticssystem"] == "piwik")
	{
		$plugin_analytics_system = "piwik";
	}
	$plugin_analytics_value = $settings->get("analytics_value",null,false);
	
	
	if(isset($_POST['analytivssave']))
	{
		Cache::clear();
		if($settings->get("analytics_system",null,false) != $_POST["analyticssystem"])
		{
			$settings->set("analytics_system",$_POST["analyticssystem"]);
		}
		
		if($plugin_analytics_value != $_POST["analyticsvalue"])
		{
			$settings->set("analytics_value",$_POST["analyticsvalue"]);
		}
		Cache::clear();
		Settings::forceReload();
		echo Language::DirectTranslate("plugin_analytics_saved");
	}
	
	
	unset($plugin_analytics_value);
	$plugin_analytics_value = $settings->get("analytics_value",null,false);
	
	if($plugin_analytics_system == "google")
	{
		Cache::clear();
		$template->assign_var("SELECTEDGOOGLE", " selected=\"selected\"");
		$template->assign_var("GOOGLEID",htmlentities($plugin_analytics_value));
		$template->show_if("SHOWGOOGLE", true);
		$template->show_if("SHOWPIWIK", false);
	}
	elseif($plugin_analytics_system == "piwik")
	{
		Cache::clear();
		$template->assign_var("SELECTEDPIWIK", " selected=\"selected\"");
		$template->assign_var("PIWIKURL",htmlentities($plugin_analytics_value));
		$template->show_if("SHOWGOOGLE", false);
		$template->show_if("SHOWPIWIK", true);
	}
	

	
	
	$template->output();
?>