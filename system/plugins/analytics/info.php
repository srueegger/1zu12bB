<?PHP
	$pluginInfo = new PluginInfo();
	$pluginInfo->path							= "analytics";
	$pluginInfo->name							= "Analytics";
	$pluginInfo->authorName						= utf8_decode("Samuel Rüegger");
	$pluginInfo->authorLink						= "http://2lounge.ch";
	$pluginInfo->version						= "2.0.0";
	$pluginInfo->license						= "CC BY-SA 3.0 CH";
	$pluginInfo->licenseUrl						= "http://creativecommons.org/licenses/by-sa/3.0/ch/";
	$pluginInfo->configurationFile				= "settings.php";
	$pluginInfo->supportedLanguages = array("de");
	$this->Add($pluginInfo);
?>