<?PHP
	$settings = Settings::getRootInstance()->specify("plugin","analytics");
	
	$template = new Template();
	if($settings->get("analytics_system") == "google" && $settings->get("analytics_value") != "")
	{
		$template->load("plugin_analytics_google");
		$template->assign_var("UAID",$settings->get("analytics_value"));
		$template->assign_var("DOMAINNAME",Settings::getInstance()->get("host"));
		$template->output();
	}
	elseif($settings->get("analytics_system") == "piwik" && $settings->get("analytics_value") != "")
	{
		$template->load("plugin_analytics_piwik");
		$plugin_analytics_piwikurl = $settings->get("analytics_value");
		if(substr($plugin_analytics_piwikurl,-5) == "https")
		{
			$plugin_analytics_piwikurl = substr($plugin_analytics_piwikurl,5);
		}
		else
		{
			$plugin_analytics_piwikurl = substr($plugin_analytics_piwikurl,4);
		}
		
		$template->assign_var("PIWIKURL",$plugin_analytics_piwikurl);
		$template->output();
	}
?>