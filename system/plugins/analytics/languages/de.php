<?PHP
	$tokens["plugin_description"]		= "F�r die Einbindung von Google Analytics oder Piwik.";
	$tokens["choose_system"]			= "W�hle dein System";
	$tokens["no_system"]				= "Kein System ausgew�hlt";
	$tokens["google_property_id"]		= "Google Property ID (Beispiel: UA-00000000-0)";
	$tokens["piwik_url"]				= "Url der Piwik Installation (Beispiel: http://piwik.url.com/)";
	$tokens["saved"]					= "Einstellungen wurden erfolgreich gespeichert";
?>