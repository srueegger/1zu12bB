<?PHP
	class Plugin_Quiz_Editor extends Editor {

		function __construct(Page $page) {
			$this->page = $page;
		}

		public function display () {
			if(isset($_POST["quizdaten"]) && $_POST["url"]==""){
				$empfaenger = "rueegger@me.com";
				$absendername = $_POST["vorname"]." ".$_POST["nachname"];
				$absendermail = $_POST["mail"];
				$betreff = "Quizwettbewerb";
				$text = "Neuer Quiz Teilnehmer: ".$_POST["vorname"]." ".$_POST["nachname"]."";
				mail($empfaenger, $betreff, $text, "From: $absendername <$absendermail>");
				echo("Herzlichen Dank für deine Teilnahme: Vergiss nicht am 24. November Ja zu stimmen!");
				exit();
			}
			if(isset($_POST["qa"]) == ""){
				echo("
				<form name=\"quizform\" method=\"post\" action=\"\">
					
					<p>Herzlich Willkommen bei unserem Quiz. Gewinne jeden Sonntag eines der folgenden Bücher:<ul><li>Wie Reiche denken und lenken“ von Ueli Mäder</li>
<li>1:12-Buch vom Denknetz und JUSO Schweiz</li>
</ul></p>
					");
					echo('<div class="progress"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%"><span class="sr-only">0% komplett</span></div></div>');
					echo ("
					<h3>Frage 1: Wer kassiert für eine Beratung einen Stundenlohn von 3’125 Franken?</h3>
					<ul>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"falsch\" id=\"false1\" /> <label for=\"false1\">Ein Starpsychologe</label>
						</li>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"falsch\" id=\"false2\" /> <label for=\"false2\">Ein bekannter Wirtschaftsanwalt</label>
						</li>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"f2\" id=\"true\" /> <label for=\"true\">Daniel Vasella, freiberuflicher Berater</label>
						</li>
					</ul>
					<p><input type=\"submit\" value=\"Weiter\" /></p>
				</form>");
			}elseif($_POST["qa"] == "falsch"){
				echo('<div class="alert alert-warning">Das war leider die falsche Antwort!</div><p><a href="./quiz.html" class="btn btn-danger btn-lg">Quiz neustarten &raquo;</a></p>');
			}elseif($_POST["qa"] == "final"){
				echo("
				<div class=\"alert alert-success\">Super du hast alle Fragen richtig beantwortet!</div>
				<p>Gib deine Daten ein um beim Wettbewerb mitzumachen. Du wirst per E-Mail kontaktiert falls du gewonnen hast!</p>");
				echo('
				<form action="" method="post">
				<p><strong>Vorname:</strong><br /><input type="text" name="vorname" value="" required="required" placeholder="Vorname" /></p>
				<p><strong>Nachname:</strong><br /><input type="text" name="nachname" value="" required="required" placeholder="Nachname" /></p>
				<p><strong>E-Mail Adresse:</strong><br /><input type="text" name="mail" value="" required="required" placeholder="E-Mail Adresse" /></p>
				<p><input type="hidden" name="url" value="" />
				<p><input type="submit" name="quizdaten" value="Speichern und Teilnehmen" /></p>
				</form>
				');
			}elseif($_POST["qa"] == "f2"){
				echo("
				<form name=\"quizform\" method=\"post\" action=\"\">
					<div class=\"alert alert-success\">Super, <strong>Daniel Vasella</strong> war die richtige Antwort! Mach weiter mit der nächsten Frage.</div>");
					echo('<div class="progress"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 14.28%"><span class="sr-only">14.28% komplett</span></div></div>');
					echo("
					<h3>Frage 2: Wer kassiert einen Monatslohn von 1,3 Mio?</h3>
					<ul>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"falsch\" id=\"false1\" /> <label for=\"false1\">Ein Fussballprofi</label>
						</li>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"falsch\" id=\"false2\" /> <label for=\"false2\">Ein bekannter Galerist</label>
						</li>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"f3\" id=\"true\" /> <label for=\"true\">Joe Jimenez, CEO von Novartis</label>
						</li>
					</ul>
					<p><input type=\"submit\" value=\"Weiter\" /></p>
				</form>");
			}elseif($_POST["qa"] == "f3"){
				echo("
				<form name=\"quizform\" method=\"post\" action=\"\">
					<div class=\"alert alert-success\">Genau, <strong>Joe Jimenez</strong> war die richtige Antwort! Mach weiter mit der nächsten Frage.</div>");
					echo('<div class="progress"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 28.57%"><span class="sr-only">28.57% komplett</span></div></div>');
					echo("
					<h3>Frage 3: Wer kassiert 12,5 Mio. im Jahr?</h3>
					<ul>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"falsch\" id=\"false1\" /> <label for=\"false1\">Ein Wirtschaftsprofessor</label>
						</li>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"falsch\" id=\"false2\" /> <label for=\"false2\">Ein Bundesrat</label>
						</li>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"f4\" id=\"true\" /> <label for=\"true\">Severin Schwan, CEO von Roche</label>
						</li>
					</ul>
					<p><input type=\"submit\" value=\"Weiter\" /></p>
				</form>");
			}elseif($_POST["qa"] == "f4"){
				echo("
				<form name=\"quizform\" method=\"post\" action=\"\">
					<div class=\"alert alert-success\">Unglaublich aber du hast Recht! <strong>Severin Schwan</strong> war die richtige Antwort! Mach weiter mit der nächsten Frage.</div>");
					echo('<div class="progress"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 42.85%"><span class="sr-only">42.85% komplett</span></div></div>');
					echo("
					<h3>Frage 4: Wer kassierte 2008 7,7 Mio. im Jahr und entliess viele Angestellte?</h3>
					<ul>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"falsch\" id=\"false1\" /> <label for=\"false1\">Ein regionaler KMU Chef</label>
						</li>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"falsch\" id=\"false2\" /> <label for=\"false2\">Ein Grossbierbrauer</label>
						</li>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"f5\" id=\"true\" /> <label for=\"true\">Hariolf Kottmann, CEO von Clariant</label>
						</li>
					</ul>
					<p><input type=\"submit\" value=\"Weiter\" /></p>
				</form>");
			}elseif($_POST["qa"] == "f5"){
				echo("
				<form name=\"quizform\" method=\"post\" action=\"\">
					<div class=\"alert alert-success\">Am Beispiel von <strong>Hariolf Kottmann</strong> sehen wir wie Abzocker im Alltag agieren! Glückwunsch, weiter so!</div>");
					echo('<div class="progress"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 57.14%"><span class="sr-only">57.14% komplett</span></div></div>');
					echo("
					<h3>Frage 5: Wer kassiert 2.5 Mio. im Jahr?</h3>
					<ul>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"falsch\" id=\"false1\" /> <label for=\"false1\">Ein Spitzenchirug</label>
						</li>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"falsch\" id=\"false2\" /> <label for=\"false2\">Eine Sekretärin</label>
						</li>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"f6\" id=\"true\" /> <label for=\"true\">Martin Strobel, CEO Baloîse-Versicherungen</label>
						</li>
					</ul>
					<p><input type=\"submit\" value=\"Weiter\" /></p>
				</form>");
			}elseif($_POST["qa"] == "f6"){
				echo("
				<form name=\"quizform\" method=\"post\" action=\"\">
					<div class=\"alert alert-success\">Schon wieder die richtige Antwort! Weiter gehts!</div>");
					echo('<div class="progress"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 71.42%"><span class="sr-only">71.42% komplett</span></div></div>');
					echo("
					<h3>Frage 6: Wer kassiert 8.5 Mio. im Jahr?</h3>
					<ul>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"falsch\" id=\"false1\" /> <label for=\"false1\">Ein Grossbauer</label>
						</li>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"falsch\" id=\"false2\" /> <label for=\"false2\">Eine Biologielaborantin</label>
						</li>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"f7\" id=\"true\" /> <label for=\"true\">Michael Mack, CEO Syngenta</label>
						</li>
					</ul>
					<p><input type=\"submit\" value=\"Weiter\" /></p>
				</form>");
			}elseif($_POST["qa"] == "f7"){
				echo("
				<form name=\"quizform\" method=\"post\" action=\"\">
					<div class=\"alert alert-success\">Genau, <strong>Michael Mack</strong> war die richtige Antwort! Mach weiter mit der nächsten Frage.</div>");
					echo('<div class="progress"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 85.71%"><span class="sr-only">85.71% komplett</span></div></div>');
					echo("
					<h3>Frage 7: Wer kassiert 3.2 Mio. pro Jahr?</h3>
					<ul>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"falsch\" id=\"false1\" /> <label for=\"false1\">Ein Regierungsrat</label>
						</li>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"falsch\" id=\"false2\" /> <label for=\"false2\">Der Rektor einer Universität</label>
						</li>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"f8\" id=\"true\" /> <label for=\"true\">Stefan Borgas, CEO Lonza und VR Syngenta</label>
						</li>
					</ul>
					<p><input type=\"submit\" value=\"Weiter zur letzten Frage!\" /></p>
				</form>");
			}elseif($_POST["qa"] == "f8"){
				echo("
				<form name=\"quizform\" method=\"post\" action=\"\">
					<div class=\"alert alert-success\">Alle Fragen bisher richtig! Super! Beantworte noch die letzte Zusatzfrage um an unserem Wettbewerb teilzunehmen.</div>");
					echo('<div class="progress"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span class="sr-only">100% komplett</span></div></div>');
					echo("
					<h3>Zusatzfrage: Wie viele Frauen gehören zu den 40 bestbezahlten ManagerInnen in der Schweiz?</h3>
					<ul>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"falsch\" id=\"false1\" /> <label for=\"false1\">20%</label>
						</li>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"falsch\" id=\"false2\" /> <label for=\"false2\">30%</label>
						</li>
						<li>
							<input type=\"radio\" name=\"qa\" value=\"final\" id=\"true\" /> <label for=\"true\">Eine: 2.5% (Nayla Hayek)</label>
						</li>
					</ul>
					<p><input type=\"submit\" value=\"Weiter\" /></p>
				</form>");
			}
		}

		public function getHeader () {
			
		}

		public function getEditableCode() {

			return '<p><input name="save" type="submit" value="'.Language::DirectTranslate("change").'" onclick="form.action=\''. UrlRewriting::GetUrlByAlias("admin/pageedit","site=".$this->page->alias).'\' ; target=\'_self\' ; return true" /></p>';
		}

		public function save(Page $newPage, Page $oldPage) {
			
		}
	}
?>