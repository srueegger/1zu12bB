<?PHP
	$tokens["plugin_description"]		= "Dieses Plugin ist f�r das 1:12 Quiz zust�ndig";
	$tokens["pagetypetitle"]			= "Neues Quiz";
	$tokens["edittitle"]				= "Welche Inhalte soll man durchsuchen k�nnen";
	$tokens["showform"]					= "Suchformular auf Seite zeigen";
	$tokens["placeholder"]				= "Suchen ...";
	$tokens["searchstart"]				= "Suche starten";
	$tokens["internpages"]				= "Interne (Admin) Seiten auch anzeigen?";
	$tokens["noresult"]					= "Text, falls keine Resultate gefunden werden:";
	$tokens["skincode"]					= "Code f�r deinen Skin:";
?>