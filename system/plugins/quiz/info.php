<?PHP
	$pluginInfo								= new PluginInfo();
	$pluginInfo->path						= "quiz";
	$pluginInfo->name						= "Quiz";
	$pluginInfo->authorName					= utf8_decode("Samuel Rüegger");
	$pluginInfo->authorLink					= "http://2lounge.ch";
	$pluginInfo->version					= "1.0.0";
	$pluginInfo->license					= "CC BY-SA 3.0 CH";
	$pluginInfo->licenseUrl					= "http://creativecommons.org/licenses/by-sa/3.0/ch/";
	$pluginInfo->supportedLanguages = array("de");
	$this->Add($pluginInfo);
?>