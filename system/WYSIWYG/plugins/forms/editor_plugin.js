(function(){
	tinymce.PluginManager.requireLangPack("forms");
	tinymce.create("tinymce.plugins.FormsPlugin",{
		init:function(a,b){
            host = b.substring(b.length-28,0);
			a.addCommand("mceForms",function(){
				a.windowManager.open({
					file:host+"admin/tinymce/forms.html?blank=true&tinymce=true",width:320+parseInt(a.getLang("forms.delta_width",0)),height:120+parseInt(a.getLang("forms.delta_height",0)),inline:1
				},{
					plugin_url:b,some_custom_arg:"custom arg"
				})
			});
		a.addButton("forms",{title:"forms.desc",cmd:"mceForms",image:host+"system/images/icons/famfamfam/application_form.png"});a.onNodeChange.add(function(d,c,e){c.setActive("forms",e.nodeName=="IMG")})},createControl:function(b,a){return null},getInfo:function(){return{longname:"Forms plugin",author:"ContentLion",authorurl:"http://tinymce.moxiecode.com"}}});tinymce.PluginManager.add("forms",tinymce.plugins.FormsPlugin)})();