var tinyMCEImageList = new Array(
	<?PHP
	  include('../system/classes/imageserver.php');
	  include('../system/classes/database.php');
	  include('../system/classes/language.php');
	  include('../system/classes/settings.php');
	  include('../system/classes/cache.php');
	  include('../system/classes/filecache.php');
	  include('../system/classes/mysql.php');
	  include('../system/classes/user.php');
	  include('../system/classes/role.php');
	  include('../system/classes/page.php');
	  include('../system/classes/sys.php');
          include('../const.php');
	  $db = new MySQL('../system/dbsettings.php');
      $db->Connect();
      DataBase::SetCurrent($db);
      $language = new Language();
      Settings::$allowFileCache = false;
	  $images = ImageServer::getImages();
	  if($images){
	    $i = 1;
  	    foreach($images as $image){
	      ?>
		  ["<?PHP echo $image->name; ?>", "<?PHP echo $image->path; ?>"]
		  <?PHP
		  if($i < count($images)){
		    echo ",";
		  }
		  $i++;
	    }
	  }
	?>
);
