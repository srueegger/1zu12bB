var tinyMCELinkList = new Array(
	<?PHP
          include('../const.php');
	  include('../system/classes/database.php');
	  include('../system/classes/language.php');
	  include('../system/classes/settings.php');
	  include('../system/classes/cache.php');
	  include('../system/classes/filecache.php');
	  include('../system/classes/mysql.php');
          include('../system/classes/user.php');
          include('../system/classes/role.php');
          include('../system/classes/page.php');
          include('../system/classes/sys.php');
          include('../system/classes/contentlionexception.php');
          include('../system/classes/accessdeniedexception.php');
          include('../system/classes/urlrewriting.php');
	  $db = new MySQL('../system/dbsettings.php');
      $db->Connect();
      $language = new Language();
	  $language->root = '../';
          $pages = Page::GetAllPages();
          $i = 1;
  	  foreach($pages as $page){
	      ?>
["<?PHP echo htmlentities($page->title); ?>", "<?PHP echo $page->GetUrl(); ?>"]
		  <?PHP
		  if($i < count($pages)){
		    echo ",";
		  }
		  $i++;
	  }
	?>
);
