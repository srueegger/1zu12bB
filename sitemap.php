<?PHP header ("Content-Type:text/xml");  ?>
<?PHP echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"; ?>
	<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<?php
	  include('const.php');
	  include('autoload.php');
	  $db = new MySQL('system/dbsettings.php');
	  $db->Connect();
	  $language = new Language();
	  $pages = $db->ReadRows("SELECT alias,priority,change_frequence,update_timestamp FROM {'dbprefix'}pages WHERE in_sitemap = 1 ORDER BY update_timestamp DESC, priority desc");
	  foreach($pages as $page){
              if($page->alias != "home"){
                  $url = UrlRewriting::GetUrlByAlias($page->alias);
              }
              else{
                  $url = Settings::getValue("host");
              }
		?>
			<url>
				<loc><?PHP echo $url; ?></loc>
				<lastmod><?PHP echo date('c',strtotime($page->update_timestamp)); ?></lastmod>
				<changefreq><?PHP echo $page->change_frequence; ?></changefreq>
				<priority><?PHP echo $page->priority; ?></priority>
			</url>
		<?PHP
	  }
	?>
</urlset>